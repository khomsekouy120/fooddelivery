import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

const kPrimaryColor = Color(0xFF145632);
const kPrimaryColorLight = Color(0xFF4CD964);
const kSecondaryColor = Color(0xFF4CD964);
const kContentColorLightTheme = Color(0xFF1D1D35);
const kContentColorDarkTheme = Color(0xFFF5FCF9);
const kContainerBackgroundColor = Color(0xFFF4F5F7);
const kWarninngColor = Color(0xFFF3BB1C);
const kChatColor = Color(0xFFFFB800);
const kErrorColor = Color(0xFFD63229);
const kContainerColor = Color(0x0DFFB800);
const kHintColor = Color(0x59000000); // 35% opacity
const kDividerColor = Color(0xFFE8E8E8);
const blackColor = Color(0xFF000000);
const kPurpleColor = Color(0x3D016D);

const kDefaultPadding = 16.0;
const kRadiusControl = 27.0;
const kRadiusSmallControl = 8.0;
const kRadiusContainer = 32.0;
const kRadiusInnerContainer = 24.0;
const kRadiusThirdContainer = 16.0;


const kBorderStrokeWidth = 0.2;
const kPhnompenhLatlng = CameraPosition(
  target: LatLng(11.562108, 104.888535),
  zoom: 14.4746
  );

const LatLng currentLocation = LatLng(11.562108, 104.888535) ;
