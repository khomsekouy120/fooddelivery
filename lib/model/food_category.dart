class FoodCategoryData {
  String image;
  String title;

  FoodCategoryData({required this.image, required this.title});
}

List<FoodCategoryData> foodCategories = [
  FoodCategoryData(image: "Chotdeal.jpeg", title: "Hot Deals"),
  FoodCategoryData(image: "Cnewonshohoz.jpeg", title: "New on FastFood"),
  FoodCategoryData(image: "Cbadgetmeal.jpeg", title: "Save Food, Save Hunger"),
  FoodCategoryData(image: "Cfavouritefood.jpeg", title: "Set Your Prefrernces Now"),
];
