class OrderData {
  String image;
  String day;
  String time;
  String name;
  int price;
  String food;

  OrderData({
    required this.name,
    required this.price,
    required this.food,
    required this.day,
    required this.image,
    required this.time,
  });
}

List<OrderData> orders = [
  OrderData(
    name: "KFC Combo",
    image: "Okfc.jpeg",
    price: 50,
    food: "1 Kacchi biriyani, 1 chilli onion, 2 Burger, 2 France Fry, 1 big pizza",
    day: "Yesterday",
    time: "3 pm",
  ),
  OrderData(
    name: "Burger King Combo",
    image: "Oburgerking.png",
    price: 60,
    food: "1 Kacchi biriyani, 1 chilli onion, 2 Burger, 2 France Fry, 1 big pizza",
    day: "Yesterday",
    time: "3 pm"
  ),
];
