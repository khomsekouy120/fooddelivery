class RestaurantData {
  String name;
  String food_type;
  bool dellivery;
  bool rescued;
  String image;
  int diskon;

  RestaurantData({
    required this.name,
    required this.food_type,
    required this.dellivery,
    required this.rescued,
    required this.image,
    required this.diskon,
  });
}

List<RestaurantData> restaurants = [
  RestaurantData(
      name: "Pizza Hut",
      food_type: "Home cook, Fast Food, Burger, Home cook, Burger",
      dellivery: true,
      rescued: true,
      image: "Rpizzahut.jpeg",
      diskon: 50),
  RestaurantData(
      name: "KFC Banani",
      food_type: "Fast food, Burger",
      dellivery: false,
      rescued: true,
      image: "Rkfc.jpeg",
      diskon: 0),
  RestaurantData(
      name: "Burger King",
      food_type: "Rescued, Fast food, Burger",
      dellivery: false,
      rescued: false,
      image: "Rburgerking.jpeg",
      diskon: 0),
  RestaurantData(
      name: "Mehfil Coffees",
      food_type: "Home cook, Beverages",
      dellivery: true,
      rescued: false,
      image: "Rmehfil.jpeg",
      diskon: 15),
  RestaurantData(
      name: "Madchef Biryani",
      food_type: "Middle Estern",
      dellivery: false,
      rescued: false,
      image: "Rmadchef.jpeg",
      diskon: 0),
  RestaurantData(
      name: "Lorem Kitchen",
      food_type: "Home cook, Fast Food, Burger, Home cook, Fast food, Burger",
      dellivery: false,
      rescued: true,
      image: "Rlorem.jpeg",
      diskon: 50),
  RestaurantData(
      name: "Madchef Biryani",
      food_type: "Rescued, Middle Eastern",
      dellivery: false,
      rescued: false,
      image: "Rmadchef.jpeg",
      diskon: 0)
];
