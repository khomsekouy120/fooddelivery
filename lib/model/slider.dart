class SliderData {
  String image;
  String title;
  String subtitle;
  String discription;
  String more;

  SliderData(
      {required this.image,
      required this.title,
      required this.subtitle,
      required this.discription,
      required this.more});
}

List<SliderData> sliders = [
  SliderData(
      image: "S1.png",
      title: "Select Preferences",
      subtitle: "For multiple users.",
      discription:
          "You can now order from multiple restaurants at the same time!",
      more: "SET THEM NEW"),
  SliderData(
      image: "S2.png",
      title: "Rescued Food",
      subtitle: "Saving food and hunger.",
      discription:
          "Left over food and supplies are gathered and sold for 50% off!",
      more: "ORDER NOW")
];
