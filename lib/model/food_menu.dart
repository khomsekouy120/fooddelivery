class FoodMenuData {
  String image;
  int price;
  bool delivery;
  int diskon;
  String restaurant;
  String description;
  String category;

  FoodMenuData({
    required this.image,
    required this.price,
    required this.delivery,
    required this.diskon,
    required this.restaurant,
    required this.description,
    required this.category,
  });
}

List<FoodMenuData> foodmenus = [
  FoodMenuData(
      image: "Mcappuccino.jpeg",
      price: 20,
      delivery: false,
      diskon: 0,
      restaurant: "Cappucino",
      description: "Suhani Restaurant",
      category: "Breakfast"),
  FoodMenuData(
      image: "Meggandchees.jpeg",
      price: 20,
      delivery: true,
      diskon: 25,
      restaurant: "Egg and cheese",
      description: "Mehfil's Place",
      category: "Breakfast"),
  FoodMenuData(
      image: "Mistahshawarma.jpeg",
      price: 10,
      delivery: false,
      diskon: 0,
      restaurant: "Strawberry Icecream",
      description: "Cream Stone",
      category: "Breakfast"),
  FoodMenuData(
      image: "Mwowmomo.jpeg",
      price: 23,
      delivery: true,
      diskon: 50,
      restaurant: "Wow! Momo",
      description: "",
      category: ""),
  FoodMenuData(
      image: "Mistahshawarma.jpeg",
      price: 15,
      delivery: false,
      diskon: 50,
      restaurant: "Istah - Shawarma",
      description: "",
      category: ""),
  FoodMenuData(
      image: "Mistahshawarma.jpeg",
      price: 20,
      delivery: false,
      diskon: 20,
      restaurant: "Istah - Shawarma",
      description: "",
      category: ""),
  FoodMenuData(
      image: "Mkfc.jpeg",
      price: 15,
      delivery: true,
      diskon: 25,
      restaurant: "KFC",
      description: "Desi, Italian, +3 more",
      category: "Popular Restaurants"),
  FoodMenuData(
      image: "Mburgerking.jpeg",
      price: 23,
      delivery: false,
      diskon: 0,
      restaurant: "Burger King",
      description: "Burgers, Bevarages",
      category: "Popular Restaurants"),
  FoodMenuData(
      image: "Meggandchees.jpeg",
      price: 24,
      delivery: true,
      diskon: 0,
      restaurant: "Paradise Restaurant",
      description: "Halal Food",
      category: "Popular Restaurants"),
];
