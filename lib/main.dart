import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/route/route_generator.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

void main() {
  ErrorWidget.builder = (FlutterErrorDetails detail) {
    bool inDebug = false;
    assert((){
      inDebug = true;
      return true;
    }());
    if (inDebug){
      return ErrorWidget(detail.exception);
    }
    return Container(
      alignment: Alignment.center,
      child: Text("Error ${detail.exception}",
        style: const TextStyle(
          color: Colors.orangeAccent,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        textAlign: TextAlign.center,
      ),
    );
  };
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Food Delivery",
      initialRoute: RouteNames.splash,
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

