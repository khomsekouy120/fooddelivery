// ignore_for_file: unnecessary_this

import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';

class AppButton extends StatelessWidget {
  final String text;
  final Color backgroundColor;
  final Widget? icon;
  final Function() onClick;
  final Color borderColor;
  final Color textColor;
  final double? width;
  final double? elevation;
  final double borderRadius;
  final bool defaultBorderRadious;

  const AppButton({
    Key? key,
    required this.text,
    required this.onClick,
    this.backgroundColor = kPrimaryColor,
    this.borderColor = Colors.transparent,
    this.icon,
    this.textColor = Colors.white,
    this.width,
    this.elevation,
    this.borderRadius = kRadiusControl,
    this.defaultBorderRadious = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 54.0,
      width: width,
      child: icon == null
          ? ElevatedButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.white, elevation: elevation ?? 4,
                padding:
                    const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                backgroundColor: backgroundColor,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: borderColor),
                  borderRadius: BorderRadius.circular(
                    this.defaultBorderRadious ? this.borderRadius : 8,
                  ),
                ),
              ),
              onPressed: onClick,
              child: Text(
                text.toUpperCase(),
                style: Theme.of(context).textTheme.button!.copyWith(
                      color: this.textColor,
                    ),
              ),
            )
          : ElevatedButton.icon(
              style: TextButton.styleFrom(
                foregroundColor: Colors.white, elevation: this.elevation ?? 4,
                padding:
                    const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                backgroundColor: this.backgroundColor,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: this.borderColor),
                  borderRadius: BorderRadius.circular(this.borderRadius),
                ),
              ),
              onPressed: onClick,
              icon: this.icon!,
              label: Text(
                text.toUpperCase(),
                style: Theme.of(context).textTheme.button!.copyWith(
                      color: this.textColor,
                    ),
              ),
            ),
    );
  }
}