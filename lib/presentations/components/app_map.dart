import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../core/constant.dart';

class AppMap extends StatefulWidget {
  final Function(CameraPosition)? onPinLocation;
  final Function()? onCameraMoveStarted;
  final Function(String)? onMapMoveCompleted;
  final LatLng? latLng;

  const AppMap({
    Key? key,
    this.onPinLocation,
    this.onCameraMoveStarted,
    this.onMapMoveCompleted,
    this.latLng,
  }) : super(key: key);

  @override
  _AppMapState createState() => _AppMapState();
}

class _AppMapState extends State<AppMap> {
  late BitmapDescriptor pinLocationIcon;
  late GoogleMapController controller;
  late CameraPosition _position;
  final Set<Marker> _markers = {};
  double lat = 11.562108;
  double lng = 104.888535;
  String city = "";
  String add = "";

  @override
  void initState() {
    super.initState();
    if (widget.latLng != null) {
      _position = CameraPosition(target: widget.latLng!, zoom: 16);
    } else {
      _position = kPhnompenhLatlng;
    }
    // setCustomMapPin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            GoogleMap(
              initialCameraPosition: kPhnompenhLatlng,
              zoomControlsEnabled: false,
              onMapCreated: (ctrl) {
                controller = ctrl;
                controller
                    .animateCamera(CameraUpdate.newCameraPosition(_position));
                setState(() {
                  _markers.add(
                    Marker(
                      markerId: const MarkerId('70'),
                      icon: pinLocationIcon,
                      position: kPhnompenhLatlng.target,
                    ),
                  );
                });
              },
              onCameraMoveStarted: widget.onCameraMoveStarted,
              onCameraMove: (position) {
                _position = position;
                lat = position.target.latitude;
                lng = position.target.longitude;
              },
              onCameraIdle: () async {
                try {
                  List<Placemark> addresses = await placemarkFromCoordinates(
                      _position.target.latitude, _position.target.longitude);
                  if (addresses.isNotEmpty) {
                    city = addresses[0].locality ?? '';
                    add = addresses[0].street ?? "";
                    widget.onMapMoveCompleted?.call(addresses[0].street ?? '');
                  }
                } catch (e) {
                  e.toString();
                }
                widget.onPinLocation?.call(_position);
              },
              padding: const EdgeInsets.only(
                bottom: 90.0,
              ),
            ),
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
          ],
        ),
      ),
      // drawer: Positioned(
      //   bottom: 20,
      //   left: 20,
      //   right: 20,
      //   child: SizedBox(
      //     width: SizeConfig.screenWidth,
      //     child: AppButton(
      //       text: "ok",
      //       onClick: () {
      //         Navigator.of(context)
      //             .pop({"lat": lat, "lng": lng, "city": city, "add": add});
      //       },
      //     ),
      //   ),
      // ),
    );
  }

  void setCustomMapPin() async {
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/images/marker.png', 100);
    pinLocationIcon = BitmapDescriptor.fromBytes(markerIcon);
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }
}