import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../core/constant.dart';

class AppScaffold extends StatelessWidget {
  final String title;
  final Widget? body;
  final Widget? footer;
  final Color bodyColor;

  const AppScaffold({
    Key? key,
    this.title = '',
    this.body,
    this.footer,
    this.bodyColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            decoration: const BoxDecoration(
              color: kContainerBackgroundColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(kRadiusContainer),
                topRight: Radius.circular(kRadiusContainer),
              ),
            ),
          ),
        ),
        Positioned.fill(
          child: Container(
            padding: const EdgeInsets.all(0),
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: kDefaultPadding,
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                    ),
                    decoration: BoxDecoration(
                        color: bodyColor,
                        borderRadius:
                            BorderRadius.circular(kRadiusInnerContainer)),
                    child: body ?? Container(),
                  ),
                ),
                footer == null
                    ? const SafeArea(
                        child: Padding(
                            padding: EdgeInsets.only(
                                top: kDefaultPadding / 2)),
                      )
                    : Container(
                        margin: const EdgeInsets.only(
                          top: kDefaultPadding,
                        ),
                        padding: const EdgeInsets.only(
                            bottom: kDefaultPadding / 2,
                            left: kDefaultPadding,
                            right: kDefaultPadding,
                            top: kDefaultPadding),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                              Radius.circular(kRadiusInnerContainer)),
                        ),
                        child: SafeArea(child: footer ?? Container()),
                      ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
