import 'package:flutter/material.dart';


class AppBottomNavbar extends StatelessWidget {

  @override 
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 40,
          height: 40,
          margin: const EdgeInsets.only(top: 10),
          decoration: BoxDecoration(  
            color: Colors.purple,
            borderRadius: BorderRadius.circular(360)
          ),
          child: IconButton(
            enableFeedback: false,
            onPressed: () {},
            icon: const Icon(
              Icons.home_filled,
              color: Colors.white,
              size: 22,
            ),
          ),
        ),
            
        IconButton(
          enableFeedback: false,
          onPressed: () {},
          icon: const Icon(
            Icons.star_outline_rounded,
            color: Colors.black,
            size: 30,
          ),
        ),
        IconButton(
          enableFeedback: false,
          onPressed: () {},
          icon: const Icon(
            Icons.card_membership,
            color: Colors.black,
            size: 30,
          ),
        ),
        IconButton(
          enableFeedback: false,
              onPressed: () {},
              icon: const Icon(
                Icons.person_outline,
                color: Colors.black,
                size: 30,
              ),
            ),
          ],
        );
  }
}