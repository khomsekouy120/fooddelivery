
import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';
import 'package:food_deliveries/presentations/screen/auth/forgot_password.dart';
import 'package:food_deliveries/presentations/screen/auth/login.dart';
import 'package:food_deliveries/presentations/screen/auth/newpassword.dart';
import 'package:food_deliveries/presentations/screen/auth/verification_code.dart';
import 'package:food_deliveries/presentations/screen/bookmaks/bookmaks.dart';
import 'package:food_deliveries/presentations/screen/detail/detail.dart';
import 'package:food_deliveries/presentations/screen/food_category/food_detail.dart';
import 'package:food_deliveries/presentations/screen/food_category/food_more_detail.dart';
import 'package:food_deliveries/presentations/screen/food_category/order_list.dart';
import 'package:food_deliveries/presentations/screen/home/home.dart';
import 'package:food_deliveries/presentations/screen/location/location.dart';
import 'package:food_deliveries/presentations/screen/order_history/order_history.dart';
import 'package:food_deliveries/presentations/screen/peyment_method/payment_method.dart';
import 'package:food_deliveries/presentations/screen/preferences/preferences.dart';
import 'package:food_deliveries/presentations/screen/profile/profile.dart';
import 'package:food_deliveries/presentations/screen/settings/settings.dart';
import 'package:food_deliveries/presentations/screen/splash_screen.dart';
import 'package:food_deliveries/presentations/screen/splash_screen/first_splash_screen.dart';
import 'package:food_deliveries/presentations/screen/splash_screen/second_splash_screen.dart';
import 'package:food_deliveries/presentations/screen/splash_screen/thirt_splash_screen.dart';
import 'package:food_deliveries/presentations/screen/welcome/welcome_screen.dart';

import '../screen/feedback/feedback.dart';
import '../screen/looking_map_delivery/delivery_map.dart';
import '../screen/notififation/notification.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case RouteNames.splash:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      case RouteNames.login:
        return MaterialPageRoute(builder: (_) => const Login());
      case RouteNames.home:
        return MaterialPageRoute(builder: (_) => const Home());
      case RouteNames.first_splash:
        return MaterialPageRoute(builder: (_) => const FirstSplashScreen());
      case RouteNames.second_splash:
        return MaterialPageRoute(builder: (_) => const SecondSplashScreen());
      case RouteNames.thirt_splash:
        return MaterialPageRoute(builder: (_) => const ThirtSplashScreen());
      case RouteNames.forgot_password:
        return MaterialPageRoute(builder: (_) => const ForgotPassword());
      case RouteNames.verification_code:
        return MaterialPageRoute(builder: (_) => const VerificationCode());
      case RouteNames.location:
        return MaterialPageRoute(builder: (_) => const Location());
      case RouteNames.newpassword:
        return MaterialPageRoute(builder: (_) => const NewPassword());
      case RouteNames.welcome_screen:
        return MaterialPageRoute(builder: (_) => const WelcomePage());
      case RouteNames.delivery_map:
        return MaterialPageRoute(builder: (_) => const DeliveryMap());
      case RouteNames.detail:
        return MaterialPageRoute(builder: (_) => const DetailPage(id: 0,));
      case RouteNames.profile:
        return MaterialPageRoute(builder: (_) =>  const Profile());
      case RouteNames.order_list:
        return MaterialPageRoute(builder: (_) => const OrderList());  
      case RouteNames.food_more_detail:
        return MaterialPageRoute(builder: (_) => const FoodMoreDetail());  

      case RouteNames.food_detail:
        return MaterialPageRoute(builder: (_) => const FoodDetail());  
      case RouteNames.preferences:
        return MaterialPageRoute(builder: (_) => const Preferences());
      case RouteNames.payment_method:
        return MaterialPageRoute(builder: (_) =>  const PaymentMethod());  

      case RouteNames.feedback:
        return MaterialPageRoute(builder: (_) => const CustomerFeedback());  
      case RouteNames.order_history:
        return MaterialPageRoute(builder: (_) => const OrderHistory());  
      case RouteNames.settings:
        return MaterialPageRoute(builder: (_) => const Settings());
      case RouteNames.notification:
        return MaterialPageRoute(builder: (_) => const MyNotification());  

      case RouteNames.bookmaks:
        return MaterialPageRoute(builder: (_) => const Bookmaks());  
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Error",
              style: TextStyle(fontSize: 20, color: Colors.red)),
        ),
      );
    });
  }
}
