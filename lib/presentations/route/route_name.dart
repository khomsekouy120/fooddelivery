import 'package:flutter/material.dart';

class RouteNames {
  static const String splash = "/";
  static const String login = "/login";
  static const String home = "/home";
  static const String first_splash = "first_splash";
  static const String second_splash = "second_splash";
  static const String thirt_splash = "thirt_splash";
  static const String forgot_password = "forgot_password";
  static const String verification_code = "verification_code";
  static const String location = "location";
  static const String newpassword = 'new_password';
  static const String welcome_screen = 'welcome_screen';
  static const String delivery_map = 'delivery_map';
  static const String search_food = 'search_food';
  static const String food_type = 'food_type';
  static const String detail = "detail";
  static const String profile = "profile";
  static const String order_list = "order_list";
  static const String food_more_detail = "food_more_detail";
  static const String food_detail = "food_detail";
  static const String payment_method = "payment_method";
  static const String preferences = "preferences";
  static const String feedback = "feedback";
  static const String order_history = "order_history";
  static const String settings = "settings";
  static const String notification = "notification";
  static const String bookmaks = "bookmaks";
}
