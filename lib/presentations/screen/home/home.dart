import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:food_deliveries/model/food_category.dart';
import 'package:food_deliveries/model/food_menu.dart';
import 'package:food_deliveries/model/order.dart';
import 'package:food_deliveries/model/restaurant.dart';
import 'package:food_deliveries/model/slider.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';
import 'package:food_deliveries/presentations/screen/detail/detail.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        child: SizedBox(
          child: LayoutBuilder(
            builder: (context, src) {
              if (src.maxWidth <= 385) {
                return Container(
                  height: MediaQuery.of(context).size.height / 1.5,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Image(
                                  image: AssetImage("assets/images/home.png")),
                              const SizedBox(
                                width: 15,
                              ),
                              SizedBox(
                                width: 230,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text(
                                      "Home",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          letterSpacing: 1),
                                    ),
                                    Text(
                                      "21-42-34, Jelupang, Hyderabad, 500072 hi ho ho ho",
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ],
                                ),
                              ),
                              const Spacer(),
                              const Image(
                                  image: AssetImage(
                                      "assets/images/home/Iheart.png"))
                            ],
                          ),
                        ),

                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(
                                context, RouteNames.search_food);
                          },
                          child: Container(
                            width: double.infinity,
                            margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const [
                                Icon(Icons.search,
                                    color: Colors.black, size: 22),
                                SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  "Search food",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        Container(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                            width: double.infinity,
                            height: 463,
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CarouselSlider.builder(
                                    options: CarouselOptions(
                                        onPageChanged: (((index, reason) {
                                          setState(() {
                                            _current = index;
                                          });
                                        })),
                                        autoPlay: true,
                                        viewportFraction: 1,
                                        aspectRatio: 2.0,
                                        height: 140,
                                        enableInfiniteScroll: true),
                                    itemCount: sliders.length,
                                    itemBuilder: (context, index, realIndex) {
                                      if (index == 0) {
                                        return Container(
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          padding: const EdgeInsets.all(10),
                                          width: double.infinity,
                                          decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            gradient: LinearGradient(
                                                begin: Alignment.centerLeft,
                                                end: Alignment.centerRight,
                                                colors: [
                                                  Color.fromARGB(
                                                      255, 79, 163, 155),
                                                  Color.fromARGB(
                                                      255, 157, 225, 209)
                                                ]),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(
                                                  width: 200,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        sliders[index].title,
                                                        style: const TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 22,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        sliders[index].subtitle,
                                                        style: const TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14),
                                                      ),
                                                      const SizedBox(
                                                        height: 15,
                                                      ),
                                                      Text(
                                                        sliders[index]
                                                            .discription,
                                                        style: const TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14),
                                                      )
                                                    ],
                                                  )),
                                              SizedBox(
                                                width: 100,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      width: 110,
                                                      height: 80,
                                                      decoration: BoxDecoration(
                                                          image: DecorationImage(
                                                              image: AssetImage(
                                                                  "assets/images/home/${sliders[index].image}"))),
                                                    ),
                                                    TextButton(
                                                        onPressed: () {},
                                                        style: TextButton
                                                            .styleFrom(
                                                          padding:
                                                              EdgeInsets.zero,
                                                          tapTargetSize:
                                                              MaterialTapTargetSize
                                                                  .shrinkWrap,
                                                        ),
                                                        child: Text(
                                                          sliders[index].more,
                                                          style:
                                                              const TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 12),
                                                        ))
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      }
                                      return Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 10),
                                        padding: const EdgeInsets.all(10),
                                        width: double.infinity,
                                        decoration: const BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors: [
                                                Color.fromARGB(
                                                    255, 180, 57, 30),
                                                Color.fromARGB(
                                                    255, 218, 147, 116)
                                              ]),
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            SizedBox(
                                                width: 200,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      sliders[index].title,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 22,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(
                                                      sliders[index].subtitle,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 14),
                                                    ),
                                                    const SizedBox(
                                                      height: 15,
                                                    ),
                                                    Text(
                                                      sliders[index]
                                                          .discription,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 14),
                                                    )
                                                  ],
                                                )),
                                            SizedBox(
                                              width: 90,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 120,
                                                    height: 80,
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                                "assets/images/home/${sliders[index].image}"))),
                                                  ),
                                                  TextButton(
                                                      onPressed: () {},
                                                      style:
                                                          TextButton.styleFrom(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        tapTargetSize:
                                                            MaterialTapTargetSize
                                                                .shrinkWrap,
                                                      ),
                                                      child: Text(
                                                        sliders[index].more,
                                                        style: const TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors.white,
                                                            fontSize: 12),
                                                      ))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: sliders.map((e) {
                                      int index = sliders.indexOf(e);
                                      return Container(
                                        width: 10,
                                        height: 10,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 4),
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: _current == index
                                                ? const Color.fromRGBO(
                                                    0, 0xFF145632, 0, 0.9)
                                                : const Color.fromRGBO(
                                                    0, 0, 0, 0.4)),
                                      );
                                    }).toList(),
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),

                                  //Food Categories
                                  SizedBox(
                                      width: double.infinity,
                                      height: 160,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children:
                                            foodCategories.map((category) {
                                          return Container(
                                            width: 100,
                                            height: 90,
                                            margin: const EdgeInsets.only(
                                                right: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              360),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              "assets/images/home/${category.image}"),
                                                          fit: BoxFit.cover)),
                                                ),
                                                Text(
                                                  category.title,
                                                  style: const TextStyle(
                                                      color: Colors.grey,
                                                      fontSize: 14),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          );
                                        }).toList(),
                                      )),

                                  //Breakfast
                                  Container(
                                      padding: const EdgeInsets.all(30),
                                      decoration: BoxDecoration(
                                          color: Colors.amber[50],
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: const [
                                              Text(
                                                "Looking for ",
                                                style: TextStyle(
                                                    color: Colors.purple,
                                                    fontSize: 20,
                                                    letterSpacing: 1),
                                              ),
                                              Text(
                                                "Breakfast?",
                                                style: TextStyle(
                                                    color: Colors.purple,
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                    letterSpacing: 1),
                                              )
                                            ],
                                          ),
                                          const Text(
                                            "Here's what you might like to taste",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 16),
                                            textAlign: TextAlign.left,
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          SizedBox(
                                            width: double.infinity,
                                            height: 260,
                                            child: ListView.builder(
                                                itemCount: foodmenus.length,
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemBuilder: (menu, index) {
                                                  //If no diskon
                                                  if (foodmenus[index]
                                                              .category ==
                                                          "Breakfast" &&
                                                      foodmenus[index].diskon ==
                                                          0 &&
                                                      !foodmenus[index]
                                                          .delivery) {
                                                    return InkWell(
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      DetailPage(
                                                                          id: index)));
                                                        },
                                                        child: Container(
                                                          width: 200,
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 10),
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                          child: Column(
                                                              children: [
                                                                Container(
                                                                  height: 130,
                                                                  width: double
                                                                      .infinity,
                                                                  margin:
                                                                      EdgeInsets
                                                                          .zero,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                          borderRadius: const BorderRadius.only(
                                                                              topLeft: Radius.circular(15),
                                                                              topRight: Radius.circular(15)),
                                                                          image: DecorationImage(
                                                                            image:
                                                                                AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                                            fit:
                                                                                BoxFit.cover,
                                                                          )),
                                                                ),
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          30,
                                                                          10,
                                                                          10),
                                                                  child: Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .restaurant,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 18,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .description,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        const SizedBox(
                                                                          height:
                                                                              20,
                                                                        ),
                                                                        Text(
                                                                          "${foodmenus[index].price} \$",
                                                                          style: const TextStyle(
                                                                              color: Colors.purple,
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 18),
                                                                        )
                                                                      ]),
                                                                )
                                                              ]),
                                                        ));
                                                  }
                                                  //If has diskon
                                                  else if (foodmenus[index]
                                                              .category ==
                                                          "Breakfast" &&
                                                      foodmenus[index].diskon >
                                                          0 &&
                                                      !foodmenus[index]
                                                          .delivery) {
                                                    return InkWell(
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      DetailPage(
                                                                          id: index)));
                                                        },
                                                        child: Container(
                                                          width: 200,
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 10),
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                          child: Column(
                                                              children: [
                                                                Container(
                                                                    height: 130,
                                                                    width: double
                                                                        .infinity,
                                                                    margin:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    decoration:
                                                                        BoxDecoration(
                                                                            borderRadius:
                                                                                const BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                                                                            image: DecorationImage(
                                                                              image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                                              fit: BoxFit.cover,
                                                                            )),
                                                                    child: Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Container(
                                                                            height:
                                                                                35,
                                                                            width:
                                                                                76,
                                                                            decoration: const BoxDecoration(
                                                                                image: DecorationImage(
                                                                              image: AssetImage("assets/images/home/Idiscount.png"),
                                                                            )),
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  "${foodmenus[index].diskon}% OFF",
                                                                                  style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),
                                                                                ),
                                                                              ],
                                                                            ))
                                                                      ],
                                                                    )),
                                                                Container(
                                                                  width: double
                                                                      .infinity,
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          30,
                                                                          10,
                                                                          20),
                                                                  child: Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .restaurant,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 18,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .description,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        const SizedBox(
                                                                          height:
                                                                              20,
                                                                        ),
                                                                        Text(
                                                                          "${foodmenus[index].price} \$",
                                                                          style: const TextStyle(
                                                                              color: Colors.purple,
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 18),
                                                                        )
                                                                      ]),
                                                                )
                                                              ]),
                                                        ));
                                                  }
                                                  //if has delivery and diskon and breakfast
                                                  else if (foodmenus[index]
                                                              .category ==
                                                          "Breakfast" &&
                                                      foodmenus[index].diskon >
                                                          0 &&
                                                      foodmenus[index]
                                                          .delivery) {
                                                    return InkWell(
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      DetailPage(
                                                                          id: index)));
                                                        },
                                                        child: Container(
                                                          width: 200,
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 10),
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                          child: Stack(
                                                              children: [
                                                                Container(
                                                                    height: 130,
                                                                    width: double
                                                                        .infinity,
                                                                    margin:
                                                                        EdgeInsets
                                                                            .zero,
                                                                    decoration:
                                                                        BoxDecoration(
                                                                            borderRadius:
                                                                                const BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15)),
                                                                            image: DecorationImage(
                                                                              image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                                              fit: BoxFit.cover,
                                                                            )),
                                                                    child: Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Container(
                                                                            height:
                                                                                35,
                                                                            width:
                                                                                76,
                                                                            decoration: const BoxDecoration(
                                                                                image: DecorationImage(
                                                                              image: AssetImage("assets/images/home/Idiscount.png"),
                                                                            )),
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: [
                                                                                Text(
                                                                                  "${foodmenus[index].diskon}% OFF",
                                                                                  style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),
                                                                                ),
                                                                              ],
                                                                            )),
                                                                      ],
                                                                    )),
                                                                Align(
                                                                  alignment:
                                                                      AlignmentDirectional
                                                                          .centerEnd,
                                                                  child:
                                                                      Container(
                                                                    margin: const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            20),
                                                                    padding: const EdgeInsets
                                                                            .symmetric(
                                                                        horizontal:
                                                                            10,
                                                                        vertical:
                                                                            5),
                                                                    decoration: BoxDecoration(
                                                                        color: Colors
                                                                            .white,
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                20),
                                                                        border: Border.all(
                                                                            color:
                                                                                Colors.grey,
                                                                            width: 0.4)),
                                                                    child:
                                                                        const Text(
                                                                      "FREE DELIVERY",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              14,
                                                                          color:
                                                                              Colors.grey),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: const EdgeInsets
                                                                          .only(
                                                                      top: 130),
                                                                  width: double
                                                                      .infinity,
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          10,
                                                                          30,
                                                                          10,
                                                                          20),
                                                                  child: Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .restaurant,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 18,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        Text(
                                                                          foodmenus[index]
                                                                              .description,
                                                                          style: const TextStyle(
                                                                              color: Colors.grey,
                                                                              fontSize: 14,
                                                                              fontWeight: FontWeight.w300),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        const SizedBox(
                                                                          height:
                                                                              20,
                                                                        ),
                                                                        Text(
                                                                          "${foodmenus[index].price} \$",
                                                                          style: const TextStyle(
                                                                              color: Colors.purple,
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 18),
                                                                        )
                                                                      ]),
                                                                )
                                                              ]),
                                                        ));
                                                  }
                                                  return Container();
                                                }),
                                          )
                                        ],
                                      )),
                                  const SizedBox(
                                    height: 30,
                                  ),

                                  //Diskon 50% head
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 280,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Diskon 50%",
                                              style: TextStyle(
                                                  color: Colors.green[700],
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            const Text(
                                              "Left over supplies and food have been used up.",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                width: 1,
                                              )),
                                          child: const Text(
                                            "ALL >",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),

                                  //Diskon 50%
                                  SizedBox(
                                    width: double.infinity,
                                    height: 260,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: foodmenus.length,
                                        itemBuilder: (context, index) {
                                          if (foodmenus[index].diskon == 50) {
                                            return InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailPage(
                                                                  id: index)));
                                                },
                                                child: Container(
                                                  width: 200,
                                                  margin: const EdgeInsets.only(
                                                      right: 10),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15),
                                                      border: Border.all(
                                                          color: Colors.grey,
                                                          width: 0.5)),
                                                  child: Column(children: [
                                                    Container(
                                                        height: 130,
                                                        width: double.infinity,
                                                        margin: EdgeInsets.zero,
                                                        decoration:
                                                            BoxDecoration(
                                                                borderRadius: const BorderRadius
                                                                        .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            15),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            15)),
                                                                image:
                                                                    DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/${foodmenus[index].image}"),
                                                                  fit: BoxFit
                                                                      .cover,
                                                                )),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                                height: 35,
                                                                width: 76,
                                                                decoration:
                                                                    const BoxDecoration(
                                                                        image:
                                                                            DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/Idiscount.png"),
                                                                )),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Text(
                                                                      "${foodmenus[index].diskon}% OFF",
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              14,
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                    ),
                                                                  ],
                                                                ))
                                                          ],
                                                        )),
                                                    Container(
                                                      width: double.infinity,
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 30, 10, 20),
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              foodmenus[index]
                                                                  .restaurant,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            Text(
                                                              foodmenus[index]
                                                                  .description,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            const SizedBox(
                                                              height: 20,
                                                            ),
                                                            Text(
                                                              "${foodmenus[index].price} \$",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .purple,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            )
                                                          ]),
                                                    )
                                                  ]),
                                                ));
                                          }
                                          return Container();
                                        }),
                                  ),
                                  const SizedBox(
                                    height: 30,
                                  ),

                                  //Order Again
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 280,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: const [
                                            Text(
                                              "Order Again",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "You Ordered from 17 Restaurants",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                width: 1,
                                              )),
                                          child: const Text(
                                            "ALL >",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  SizedBox(
                                      width: double.infinity,
                                      height: 140,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: orders.map((order) {
                                          return Container(
                                            padding: const EdgeInsets.all(15),
                                            width: 300,
                                            height: 60,
                                            margin: const EdgeInsets.only(
                                                right: 10),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                    width: 0.5,
                                                    color: Colors.grey)),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              360),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              "assets/images/home/${order.image}"),
                                                          fit: BoxFit.cover)),
                                                ),
                                                const SizedBox(
                                                  width: 10,
                                                ),
                                                SizedBox(
                                                    width: 200,
                                                    child: Column(
                                                      children: [
                                                        SizedBox(
                                                            width: 200,
                                                            height: 40,
                                                            child: Row(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                SizedBox(
                                                                  width: 160,
                                                                  child: Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        order
                                                                            .name,
                                                                        style: const TextStyle(
                                                                            fontSize:
                                                                                18,
                                                                            fontWeight:
                                                                                FontWeight.w200),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      ),
                                                                      Text(
                                                                        "${order.day} ${order.time}",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                14,
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                            color: Colors.grey[400]),
                                                                        textAlign:
                                                                            TextAlign.start,
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "${order.price}k",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          400],
                                                                      fontSize:
                                                                          18),
                                                                )
                                                              ],
                                                            )),
                                                        const SizedBox(
                                                          height: 20,
                                                        ),
                                                        Text(
                                                          order.food,
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .grey[600]),
                                                          maxLines: 2,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ],
                                                    )),
                                              ],
                                            ),
                                          );
                                        }).toList(),
                                      )),
                                  const SizedBox(
                                    height: 40,
                                  ),

                                  //Popular restaurants
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 280,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: const [
                                            Text(
                                              "Popular Restaurants",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "Some of them offer rescued food.",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                width: 1,
                                              )),
                                          child: const Text(
                                            "ALL >",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    width: double.infinity,
                                    height: 260,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: foodmenus.length,
                                        itemBuilder: (context, index) {
                                          //If no diskon
                                          if (foodmenus[index].category ==
                                                  "Popular Restaurants" &&
                                              foodmenus[index].diskon == 0 &&
                                              !foodmenus[index].delivery) {
                                            return InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailPage(
                                                                  id: index)));
                                                },
                                                child: Container(
                                                  width: 200,
                                                  margin: const EdgeInsets.only(
                                                      right: 10),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        width: 0.4),
                                                  ),
                                                  child: Column(children: [
                                                    Container(
                                                      height: 130,
                                                      width: double.infinity,
                                                      margin: EdgeInsets.zero,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              const BorderRadius
                                                                      .only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          15),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          15)),
                                                          image:
                                                              DecorationImage(
                                                            image: AssetImage(
                                                                "assets/images/home/${foodmenus[index].image}"),
                                                            fit: BoxFit.cover,
                                                          )),
                                                    ),
                                                    Container(
                                                      width: double.infinity,
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 30, 10, 10),
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              foodmenus[index]
                                                                  .restaurant,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            Text(
                                                              foodmenus[index]
                                                                  .description,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            const SizedBox(
                                                              height: 20,
                                                            ),
                                                            Text(
                                                              "${foodmenus[index].price} \$",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .purple,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            )
                                                          ]),
                                                    )
                                                  ]),
                                                ));
                                          }
                                          //If has diskon
                                          else if (foodmenus[index].category ==
                                                  "Popular Restaurants" &&
                                              foodmenus[index].diskon > 0 &&
                                              !foodmenus[index].delivery) {
                                            return InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailPage(
                                                                  id: index)));
                                                },
                                                child: Container(
                                                  width: 200,
                                                  margin: const EdgeInsets.only(
                                                      right: 10),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    border: Border.all(
                                                        color: Colors.grey,
                                                        width: 0.4),
                                                  ),
                                                  child: Column(children: [
                                                    Container(
                                                        height: 130,
                                                        width: double.infinity,
                                                        margin: EdgeInsets.zero,
                                                        decoration:
                                                            BoxDecoration(
                                                                borderRadius: const BorderRadius
                                                                        .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            15),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            15)),
                                                                image:
                                                                    DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/${foodmenus[index].image}"),
                                                                  fit: BoxFit
                                                                      .cover,
                                                                )),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                                height: 35,
                                                                width: 76,
                                                                decoration:
                                                                    const BoxDecoration(
                                                                        image:
                                                                            DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/Idiscount.png"),
                                                                )),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Text(
                                                                      "${foodmenus[index].diskon}% OFF",
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              14,
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                    ),
                                                                  ],
                                                                ))
                                                          ],
                                                        )),
                                                    Container(
                                                      width: double.infinity,
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 30, 10, 20),
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              foodmenus[index]
                                                                  .restaurant,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            Text(
                                                              foodmenus[index]
                                                                  .description,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            const SizedBox(
                                                              height: 20,
                                                            ),
                                                            Text(
                                                              "${foodmenus[index].price} \$",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .purple,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            )
                                                          ]),
                                                    )
                                                  ]),
                                                ));
                                          }
                                          //if has delivery and diskon and breakfast
                                          else if (foodmenus[index].category ==
                                                  "Popular Restaurants" &&
                                              foodmenus[index].diskon > 0 &&
                                              foodmenus[index].delivery) {
                                            return InkWell(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              DetailPage(
                                                                  id: index)));
                                                },
                                                child: Container(
                                                  width: 200,
                                                  margin: const EdgeInsets.only(
                                                      right: 10),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: Colors.grey,
                                                          width: 0.4),
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: Stack(children: [
                                                    Container(
                                                        height: 130,
                                                        width: double.infinity,
                                                        margin: EdgeInsets.zero,
                                                        decoration:
                                                            BoxDecoration(
                                                                borderRadius: const BorderRadius
                                                                        .only(
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            15),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            15)),
                                                                image:
                                                                    DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/${foodmenus[index].image}"),
                                                                  fit: BoxFit
                                                                      .cover,
                                                                )),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                                height: 35,
                                                                width: 76,
                                                                decoration:
                                                                    const BoxDecoration(
                                                                        image:
                                                                            DecorationImage(
                                                                  image: AssetImage(
                                                                      "assets/images/home/Idiscount.png"),
                                                                )),
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Text(
                                                                      "${foodmenus[index].diskon}% OFF",
                                                                      style: const TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              14,
                                                                          fontWeight:
                                                                              FontWeight.w600),
                                                                    ),
                                                                  ],
                                                                )),
                                                          ],
                                                        )),
                                                    Align(
                                                      alignment:
                                                          AlignmentDirectional
                                                              .centerEnd,
                                                      child: Container(
                                                        margin: const EdgeInsets
                                                            .only(right: 20),
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal: 10,
                                                                vertical: 5),
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20),
                                                            border: Border.all(
                                                                color:
                                                                    Colors.grey,
                                                                width: 0.4)),
                                                        child: const Text(
                                                          "FREE DELIVERY",
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      // alignment: AlignmentDirectional.bottomStart,
                                                      width: double.infinity,
                                                      margin:
                                                          const EdgeInsets.only(
                                                              top: 130),
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 30, 10, 20),
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              foodmenus[index]
                                                                  .restaurant,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            Text(
                                                              foodmenus[index]
                                                                  .description,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                            const SizedBox(
                                                              height: 20,
                                                            ),
                                                            Text(
                                                              "${foodmenus[index].price} \$",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .purple,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 18),
                                                            )
                                                          ]),
                                                    )
                                                  ]),
                                                ));
                                          }
                                          return Container();
                                        }),
                                  ),
                                  const SizedBox(
                                    height: 30,
                                  ),

                                  //all restaurants
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 280,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: const [
                                            Text(
                                              "All Restaurants",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "256 Restaurants near you",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                width: 1,
                                              )),
                                          child: const Text(
                                            "ALL >",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),

                                  //Option user choose
                                  Row(
                                    children: [
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: const Text(
                                            "Free Delivery",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: Colors.grey[200],
                                          ),
                                          child: const Text(
                                            "Rescued",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: const EdgeInsets.all(15),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            color: Colors.grey[200],
                                          ),
                                          child: const Text(
                                            "Offer",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 30,
                                  ),

                                  //All restaurant
                                  Column(
                                    children: restaurants.map((restaurant) {
                                      return Container(
                                        margin:
                                            const EdgeInsets.only(bottom: 30),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: 60,
                                              height: 90,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          360),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          "assets/images/home/${restaurant.image}"))),
                                            ),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            SizedBox(
                                              width: 250,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    restaurant.name,
                                                    style: const TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w300),
                                                  ),
                                                  const SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    restaurant.food_type,
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 20,
                                                  ),
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            width: 0.1)),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                  const SizedBox(
                                    height: 30,
                                  ),

                                  //Botton view all restaurant
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      width: double.infinity,
                                      padding: const EdgeInsets.all(15),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          border: Border.all(
                                            width: 1,
                                          )),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: const [
                                          Text(
                                            "VIEW ALL RESTAURANTS",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Icon(Icons.arrow_forward_ios,
                                              color: Colors.grey, size: 16)
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 40,
                                  ),
                                ],
                              ),
                            )),

                        //Bottom navigation bar
                        Container(
                          width: double.infinity,
                          height: 66,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromARGB(184, 182, 182, 182),
                                offset: Offset(
                                  0.0,
                                  1.0,
                                ),
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      color: Colors.purple,
                                      borderRadius: BorderRadius.circular(360)),
                                  width: 40,
                                  height: 40,
                                  child: SvgPicture.asset(
                                    "assets/images/bottom_navbar/Discovery.svg",
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  width: 40,
                                  height: 40,
                                  child: SvgPicture.asset(
                                    "assets/images/bottom_navbar/Star.svg",
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  width: 50,
                                  height: 50,
                                  child: const Icon(
                                    Icons.shopping_cart_outlined,
                                    size: 30,
                                    color: Colors.black87,
                                  ),
                                  // child:  SvgPicture.asset("assets/images/bottom_navbar/Bag - 3.svg", color: Colors.black,),
                                ),
                              ),
                              InkWell(
                                onTap: () {},
                                child: Container(
                                  padding: const EdgeInsets.all(5),
                                  width: 40,
                                  height: 40,
                                  child: SvgPicture.asset(
                                    "assets/images/bottom_navbar/User.svg",
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ]),
                );
              } else if (src.maxWidth >= 385) {
                return Container(
                  height: MediaQuery.of(context).size.height / 1.3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Image(image: AssetImage("assets/images/home.png")),
                            const SizedBox(width: 15,),
                            SizedBox(
                              width: 230,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text("Home", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16, letterSpacing: 1),),
                                  Text("21-42-34, Jelupang, Hyderabad, 500072 hi ho ho ho", overflow: TextOverflow.ellipsis,)
                                ],
                              ),
                            ),
                            const Spacer(),
                            const Image(image: AssetImage("assets/images/home/Iheart.png"))
                          ],
                        ),
                      ),
                      
                      InkWell(
                        onTap: () { 
                          Navigator.pushNamed(context, RouteNames.search_food);
                        },
                        child: Container(
                          width: double.infinity,
                          margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Icon(Icons.search, color: Colors.black, size: 22),
                              SizedBox(width: 20,),
                              Text("Search food", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 20,),

                      Container(
                        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        width: double.infinity,
                        height: 463,
                      child: SingleChildScrollView(
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [

                          CarouselSlider.builder(
                            options: CarouselOptions(
                              onPageChanged: (((index, reason) {
                                setState(() {
                                  _current = index;
                                });
                              })),
                              autoPlay: true,
                              viewportFraction: 1,
                              aspectRatio: 2.0,
                              height: 140,
                              enableInfiniteScroll: true
                            ),
                            itemCount: sliders.length,
                            itemBuilder: (context, index, realIndex) {
                              if (index == 0) {
                                return Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                  padding: const EdgeInsets.all(10),
                                  width: double.infinity,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [Color.fromARGB(255, 79, 163, 155), Color.fromARGB(255, 157, 225, 209)]),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width: 200,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(sliders[index].title, style: const TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),),
                                            Text(sliders[index].subtitle, style: const TextStyle(color: Colors.white, fontSize: 14),),
                                            const SizedBox(height: 15,),
                                            Text(sliders[index].discription, style: const TextStyle(color: Colors.white, fontSize: 14),)
                                          ],
                                        )
                                      ),
                                      SizedBox(
                                        width: 100,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              width: 110,
                                              height: 80,
                                              decoration: BoxDecoration(  
                                                image: DecorationImage(
                                                  image: AssetImage("assets/images/home/${sliders[index].image}")
                                                )
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: (){}, 
                                              style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                              ),
                                              child: Text(
                                                sliders[index].more,
                                                style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12), 
                                              )
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              }
                              return Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 10),
                                  padding: const EdgeInsets.all(10),
                                  width: double.infinity,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [Color.fromARGB(255, 180, 57, 30), Color.fromARGB(255, 218, 147, 116)]),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width: 200,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(sliders[index].title, style: const TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),),
                                            Text(sliders[index].subtitle, style: const TextStyle(color: Colors.white, fontSize: 14),),
                                            const SizedBox(height: 15,),
                                            Text(sliders[index].discription, style: const TextStyle(color: Colors.white, fontSize: 14),)
                                          ],
                                        )
                                      ),
                                      SizedBox(
                                        width: 90,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              width: 120,
                                              height: 80,
                                              decoration: BoxDecoration(  
                                                image: DecorationImage(
                                                  image: AssetImage("assets/images/home/${sliders[index].image}")
                                                )
                                              ),
                                            ),
                                            TextButton(
                                              onPressed: (){}, 
                                              style: TextButton.styleFrom(
                                                padding: EdgeInsets.zero,
                                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                              ),
                                              child: Text(
                                                sliders[index].more,
                                                style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 12), 
                                              )
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                            },
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: sliders.map((e) {
                              int index = sliders.indexOf(e);
                              return Container(
                                width: 10,
                                height: 10,
                                margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 4),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index ? const Color.fromRGBO(0, 0xFF145632, 0, 0.9) : const Color.fromRGBO(0, 0, 0, 0.4 )
                                ),
                              );
                            }).toList(),
                          ),
                          const SizedBox(height: 50,),

                          //Food Categories
                          SizedBox(
                            width: double.infinity,
                            height: 160,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: foodCategories.map((category) {
                                return Container(
                                  width: 100,
                                  height: 90,
                                  margin: const EdgeInsets.only(right: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(360),
                                          image: DecorationImage(  
                                            image: AssetImage("assets/images/home/${category.image}"),
                                            fit: BoxFit.cover
                                          )
                                        ),
                                      ),
                                      Text(category.title, style: const TextStyle(color: Colors.grey, fontSize: 14), textAlign: TextAlign.center,),
                                    ],
                                  ),
                                );
                              }).toList(),
                            )
                          ),

                          //Breakfast
                          Container(
                            padding: const EdgeInsets.all(30),
                            decoration: BoxDecoration(
                              color: Colors.amber[50],
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: const[
                                    Text("Looking for ", style: TextStyle(color: Colors.purple, fontSize: 20, letterSpacing: 1),),
                                    Text("Breakfast?", style: TextStyle(color: Colors.purple, fontSize: 20, fontWeight: FontWeight.bold, letterSpacing: 1),)
                                  ],
                                ),
                                const Text("Here's what you might like to taste", style: TextStyle(color: Colors.grey, fontSize: 16), textAlign: TextAlign.left,),
                                const SizedBox(height: 20,),
                                SizedBox(
                                  width: double.infinity,
                                  height: 260,
                                  child: ListView.builder(
                                    itemCount: foodmenus.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder:(menu, index){
                                      //If no diskon
                                      if (foodmenus[index].category == "Breakfast" && foodmenus[index].diskon == 0 && !foodmenus[index].delivery) {
                                        return InkWell(
                                          onTap: () {
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                          },
                                          child: Container(
                                            width: 200,
                                            margin: const EdgeInsets.only(right: 10),
                                            decoration: BoxDecoration(  
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(20)
                                            ),
                                            child: Column(children: [
                                              Container(
                                                height: 130,
                                                width: double.infinity,
                                                margin: EdgeInsets.zero,
                                                decoration: BoxDecoration(
                                                  borderRadius: const BorderRadius.only(
                                                    topLeft: Radius.circular(15),
                                                    topRight: Radius.circular(15)
                                                  ),
                                                  image: DecorationImage(  
                                                    image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                    fit: BoxFit.cover,
                                                  )
                                                ),
                                              ),
                                              Container(
                                                width: double.infinity,
                                                padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    const SizedBox(height: 20,),
                                                    Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                                ]),
                                              )
                                            ]),
                                          )
                                        );
                                        
                                      }  
                                      //If has diskon
                                      else if (foodmenus[index].category == "Breakfast" && foodmenus[index].diskon > 0 && !foodmenus[index].delivery) {
                                        return InkWell(
                                          onTap: () {
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                          },
                                          child: Container(
                                            width: 200,
                                            margin: const EdgeInsets.only(right: 10),
                                            decoration: BoxDecoration(  
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(20)
                                            ),
                                            child: Column(children: [
                                              Container(
                                                height: 130,
                                                width: double.infinity,
                                                margin: EdgeInsets.zero,
                                                decoration: BoxDecoration(
                                                  borderRadius: const BorderRadius.only(
                                                    topLeft: Radius.circular(15),
                                                    topRight: Radius.circular(15)
                                                  ),
                                                  image: DecorationImage(  
                                                    image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                    fit: BoxFit.cover,
                                                  )
                                                ),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      height: 35,
                                                      width: 76,
                                                      decoration: const BoxDecoration(
                                                        image: DecorationImage(image: AssetImage("assets/images/home/Idiscount.png"),)
                                                      ),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: [
                                                        Text("${foodmenus[index].diskon}% OFF", style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),),
                                                      ],)
                                                    )
                                                    
                                                  ],        
                                                )
                                              ),
                                              Container(
                                                width: double.infinity,
                                                padding: const EdgeInsets.fromLTRB(10, 30, 10, 20),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    const SizedBox(height: 20,),
                                                    Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                                ]),
                                              )
                                            ]),
                                          )
                                        );
                                        
                                      }
                                      //if has delivery and diskon and breakfast
                                      else if (foodmenus[index].category == "Breakfast" && foodmenus[index].diskon > 0 && foodmenus[index].delivery) {
                                        return InkWell(
                                          onTap: () {
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                          },
                                          child: Container(
                                            width: 200,
                                            margin: const EdgeInsets.only(right: 10),
                                            decoration: BoxDecoration(  
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(20)
                                            ),
                                            child: Stack(children: [
                                              Container(
                                                height: 130,
                                                width: double.infinity,
                                                margin: EdgeInsets.zero,
                                                decoration: BoxDecoration(
                                                  borderRadius: const BorderRadius.only(
                                                    topLeft: Radius.circular(15),
                                                    topRight: Radius.circular(15)
                                                  ),
                                                  image: DecorationImage(  
                                                    image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                                    fit: BoxFit.cover,
                                                  )
                                                ),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      height: 35,
                                                      width: 76,
                                                      decoration: const BoxDecoration(
                                                        image: DecorationImage(image: AssetImage("assets/images/home/Idiscount.png"),)
                                                      ),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: [
                                                        Text("${foodmenus[index].diskon}% OFF", style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),),
                                                      ],)
                                                    ),                            	
                                                  ],        
                                                )
                                              ),
                                              Align(
                                                alignment: AlignmentDirectional.centerEnd,
                                                child: Container(
                                                  margin: const EdgeInsets.only(right: 20),
                                                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                                  decoration: BoxDecoration(  
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(20),
                                                    border: Border.all( 
                                                      color: Colors.grey,
                                                      width: 0.4
                                                    )
                                                  ),
                                                  child: const Text("FREE DELIVERY", style: TextStyle(fontSize: 14, color: Colors.grey),),
                                                ),
                                              ),
                                              Container(
                                                margin: const EdgeInsets.only(top: 130),
                                                width: double.infinity,
                                                padding: const EdgeInsets.fromLTRB(10, 30, 10, 20),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                                    const SizedBox(height: 20,),
                                                    Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                                ]),
                                              )
                                            ]),
                                          )
                                        );
                                        
                                      }
                                      return Container();
                                    }
                                  ),
                                )
                              ],
                            )
                          ),
                          const SizedBox(height: 30,),

                          //Diskon 50% head
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 280,
                                child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Diskon 50%", style: TextStyle(color: Colors.green[700], fontSize: 20, fontWeight: FontWeight.w500),) ,
                                  const SizedBox(height: 10,),
                                  const Text("Left over supplies and food have been used up.", style: TextStyle(color: Colors.grey, fontSize: 16),) 
                                ],
                              ),
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      width: 1,
                                    )
                                  ),
                                  child: const Text("ALL >", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 10,),

                          //Diskon 50%
                          SizedBox(
                            width: double.infinity,
                            height: 260,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: foodmenus.length,
                              itemBuilder: (context, index){
                                if (foodmenus[index].diskon == 50) {
                                  return InkWell(
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                    },
                                    child: Container(
                                      width: 200,
                                      margin: const EdgeInsets.only(right: 10),
                                      decoration: BoxDecoration(  
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(15),
                                        border: Border.all(
                                          color: Colors.grey,
                                          width: 0.5
                                        )
                                      ),
                                      child: Column(children: [
                                        Container(
                                          height: 130,
                                          width: double.infinity,
                                          margin: EdgeInsets.zero,
                                          decoration: BoxDecoration(
                                            borderRadius: const BorderRadius.only(
                                              topLeft: Radius.circular(15),
                                              topRight: Radius.circular(15)
                                            ),
                                            image: DecorationImage(  
                                              image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                              fit: BoxFit.cover,
                                            )
                                          ),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                height: 35,
                                                width: 76,
                                                decoration: const BoxDecoration(
                                                  image: DecorationImage(image: AssetImage("assets/images/home/Idiscount.png"),)
                                                ),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    Text("${foodmenus[index].diskon}% OFF", style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),),
                                                ],)
                                              )
                                                    
                                            ],        
                                          )
                                        ),
                                        Container(
                                          width: double.infinity,
                                          padding: const EdgeInsets.fromLTRB(10, 30, 10, 20),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                              Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                              const SizedBox(height: 20,),
                                              Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                          ]),
                                        )
                                      ]),
                                    )
                                  );
                                  
                                } 
                                return Container();    
                              }
                            ),
                          ),
                          const SizedBox(height: 30,),

                          //Order Again
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 280,
                                child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text("Order Again", style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),) ,
                                  SizedBox(height: 10,),
                                  Text("You Ordered from 17 Restaurants", style: TextStyle(color: Colors.grey, fontSize: 16),) 
                                ],
                              ),
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      width: 1,
                                    )
                                  ),
                                  child: const Text("ALL >", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 20,),
                          SizedBox(
                            width: double.infinity,
                            height: 140,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: orders.map((order) {
                                return Container(
                                  padding: const EdgeInsets.all(15),
                                  width: 300,
                                  height: 60,
                                  margin: const EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey
                                    )
                                  ),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 50,
                                        width: 50,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(360),
                                          image: DecorationImage(  
                                            image: AssetImage("assets/images/home/${order.image}"),
                                            fit: BoxFit.cover
                                          )
                                        ),
                                      ),
                                      const SizedBox(width: 10,),
                                      SizedBox(
                                        width: 200,
                                        child: Column(
                                          children: [
                                            SizedBox(
                                              width: 200,
                                              height: 40,
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  SizedBox(
                                                    width: 160,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(order.name, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w200), textAlign: TextAlign.start,),
                                                        Text("${order.day} ${order.time}", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey[400]), textAlign: TextAlign.start,)
                                                      ],
                                                    ),
                                                  ),
                                                  Text("${order.price}k", style: TextStyle(color: Colors.grey[400], fontSize: 18),)
                                                ],
                                              )
                                            ),
                                            const SizedBox(height: 20,),
                                            Text(order.food, style: TextStyle(color: Colors.grey[600]), maxLines: 2, overflow: TextOverflow.ellipsis,),
                                          ],
                                        )
                                      ),
                                    ],
                                  ),
                                );
                              }).toList(),
                            )
                          ),
                          const SizedBox(height: 40,),


                          //Popular restaurants
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 280,
                                child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text("Popular Restaurants", style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),) ,
                                  SizedBox(height: 10,),
                                  Text("Some of them offer rescued food.", style: TextStyle(color: Colors.grey, fontSize: 16),) 
                                ],
                              ),
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      width: 1,
                                    )
                                  ),
                                  child: const Text("ALL >", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 10,),
                          SizedBox(
                            width: double.infinity,
                            height: 260,
                            child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: foodmenus.length,
                            itemBuilder: (context, index){
                              //If no diskon
                              if (foodmenus[index].category == "Popular Restaurants" && foodmenus[index].diskon == 0 && !foodmenus[index].delivery) {
                                return InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                  },
                                  child: Container(
                                    width: 200,
                                    margin: const EdgeInsets.only(right: 10),
                                    decoration: BoxDecoration(  
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 0.4
                                      ),
                                    ),
                                    child: Column(children: [
                                      Container(
                                        height: 130,
                                        width: double.infinity,
                                        margin: EdgeInsets.zero,
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(15),
                                            topRight: Radius.circular(15)
                                          ),
                                          image: DecorationImage(  
                                            image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                            fit: BoxFit.cover,
                                          )
                                        ),
                                      ),
                                      Container(
                                        width: double.infinity,
                                        padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                            Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                            const SizedBox(height: 20,),
                                            Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                          ]),
                                        )
                                      ]
                                    ),
                                  )
                                );
                                
                              }  
                              //If has diskon
                              else if (foodmenus[index].category == "Popular Restaurants" && foodmenus[index].diskon > 0 && !foodmenus[index].delivery) {
                                return InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                  },
                                  child: Container(
                                    width: 200,
                                    margin: const EdgeInsets.only(right: 10),
                                    decoration: BoxDecoration(  
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 0.4
                                      ),
                                    ),
                                    child: Column(children: [
                                      Container(
                                        height: 130,
                                        width: double.infinity,
                                        margin: EdgeInsets.zero,
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(15),
                                            topRight: Radius.circular(15)
                                          ),
                                          image: DecorationImage(  
                                            image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                            fit: BoxFit.cover,
                                          )
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              height: 35,
                                              width: 76,
                                              decoration: const BoxDecoration(
                                                image: DecorationImage(image: AssetImage("assets/images/home/Idiscount.png"),)
                                              ),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                Text("${foodmenus[index].diskon}% OFF", style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),),
                                              ],)
                                            )

                                          ],                  
                                        )
                                      ),
                                      Container(
                                        width: double.infinity,
                                        padding: const EdgeInsets.fromLTRB(10, 30, 10, 20),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                            Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                            const SizedBox(height: 20,),
                                            Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                        ]),
                                      )
                                    ]),
                                  )
                                );
                                
                              }
                              //if has delivery and diskon and breakfast
                              else if (foodmenus[index].category == "Popular Restaurants" && foodmenus[index].diskon > 0 && foodmenus[index].delivery) {
                                return InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> DetailPage(id: index)));
                                  },
                                  child: Container(
                                    width: 200,
                                    margin: const EdgeInsets.only(right: 10),
                                    decoration: BoxDecoration(  
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 0.4
                                      ),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(20)
                                    ),
                                    child: Stack(children: [
                                      Container(
                                        height: 130,
                                        width: double.infinity,
                                        margin: EdgeInsets.zero,
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(15),
                                            topRight: Radius.circular(15)
                                          ),
                                          image: DecorationImage(  
                                            image: AssetImage("assets/images/home/${foodmenus[index].image}"),
                                            fit: BoxFit.cover,
                                          )
                                        ),
                                        child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 35,
                                            width: 76,
                                            decoration: const BoxDecoration(
                                              image: DecorationImage(image: AssetImage("assets/images/home/Idiscount.png"),)
                                            ),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("${foodmenus[index].diskon}% OFF", style: const TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600),),
                                              ],
                                            )
                                          ),                            	

                                        ],        
                                      )
                                    ),
                                    Align(
                                      alignment: AlignmentDirectional.centerEnd,
                                      child: Container(
                                        margin: const EdgeInsets.only(right: 20),
                                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                        decoration: BoxDecoration(  
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(20),
                                          border: Border.all( 
                                            color: Colors.grey,
                                            width: 0.4
                                          )
                                        ),
                                        child: const Text("FREE DELIVERY", style: TextStyle(fontSize: 14, color: Colors.grey),),
                                      ),
                                    ),
                                    Container(
                                      // alignment: AlignmentDirectional.bottomStart,
                                      width: double.infinity,
                                      margin: const EdgeInsets.only(top: 130),
                                      padding: const EdgeInsets.fromLTRB(10, 30, 10, 20),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(foodmenus[index].restaurant, style: const TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                          Text(foodmenus[index].description, style: const TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w300), textAlign: TextAlign.right,),
                                          const SizedBox(height: 20,),
                                          Text("${foodmenus[index].price} \$", style: const TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 18),)
                                        ]),
                                    )
                                  ]),
                                )
                                );
                                
                              }
                              return Container();
                              }
                            ),
                          ),
                          const SizedBox(height: 30,),
                          
                          //all restaurants
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 280,
                                child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [
                                  Text("All Restaurants", style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w500),) ,
                                  SizedBox(height: 10,),
                                  Text("256 Restaurants near you", style: TextStyle(color: Colors.grey, fontSize: 16),) 
                                ],
                              ),
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      width: 1,
                                    )
                                  ),
                                  child: const Text("ALL >", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 20,),

                          //Option user choose
                          Row(
                            children: [
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: const Text("Free Delivery", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              ),
                              const SizedBox(width: 10,),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.grey[200],
                                  ),
                                  child: const Text("Rescued", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              ),
                              const SizedBox(width: 10,),
                              InkWell(
                                onTap: () { },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.grey[200],
                                  ),
                                  child: const Text("Offer", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 30,),

                          //All restaurant
                          Column(
                            children: restaurants.map((restaurant) {
                              return Container(
                                margin: const EdgeInsets.only(bottom: 30),
                                child: Row(
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 90,
                                      decoration: BoxDecoration( 
                                        borderRadius: BorderRadius.circular(360), 
                                        image: DecorationImage(
                                          image: AssetImage("assets/images/home/${restaurant.image}")
                                        )
                                      ),
                                    ),
                                    const SizedBox(width: 20,),
                                    SizedBox(
                                      width: 250,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(restaurant.name, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w300),),
                                          const SizedBox(height: 5,),
                                          Text(restaurant.food_type, style: const TextStyle(fontSize: 16,),),
                                          const SizedBox(height: 20,),
                                          Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(width: 0.1)
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                          ),
                          const SizedBox(height: 30,),

                          //Botton view all restaurant
                          InkWell(
                            onTap: () { },
                            child: Container(
                              width: double.infinity,
                              padding: const EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 1,
                                )
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text("VIEW ALL RESTAURANTS", style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold),),
                                  SizedBox(width: 20,),
                                  Icon(Icons.arrow_forward_ios, color: Colors.grey, size: 16)
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(height: 40,),
                        ],
                      ),)),
                          
                      //Bottom navigation bar     
                      Container(
                        width: double.infinity,
                        height: 66,
                        decoration: const BoxDecoration(  
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(184, 182, 182, 182),
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                              blurRadius: 5.0,
                              spreadRadius: 1.0,
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                              },
                              child: Container(  
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(  
                                  color: Colors.purple,
                                  borderRadius: BorderRadius.circular(360)
                                ),
                                width: 40,
                                height: 40,
                                child:  SvgPicture.asset("assets/images/bottom_navbar/Discovery.svg", color: Colors.white,),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                
                              },
                              child: Container(  
                                padding: const EdgeInsets.all(5),
                                width: 40,
                                height: 40,
                                child:  SvgPicture.asset("assets/images/bottom_navbar/Star.svg", color: Colors.black,),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                
                              },
                              child: Container(  
                                padding: const EdgeInsets.all(5),
                                width: 50,
                                height: 50,
                                child: const Icon(Icons.shopping_cart_outlined, size: 30, color: Colors.black87,),
                                // child:  SvgPicture.asset("assets/images/bottom_navbar/Bag - 3.svg", color: Colors.black,),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                
                              },
                              child: Container(  
                                padding: const EdgeInsets.all(5),
                                width: 40,
                                height: 40,
                                child:  SvgPicture.asset("assets/images/bottom_navbar/User.svg", color: Colors.black,),
                              ),
                            ),
                            
                          ],
                        ),
                      )
                    ]
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
