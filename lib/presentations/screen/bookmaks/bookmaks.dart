import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../route/route_name.dart';

class Bookmaks extends StatefulWidget {
  const Bookmaks({super.key});

  @override
  State<Bookmaks> createState() => _BookmaksState();
}

class _BookmaksState extends State<Bookmaks> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container (  
        padding: const EdgeInsets.all(15),
        child: Column(  
          children: [  
            Container(  
              margin: const EdgeInsets.only(top: 50),
              child: Row( 
                children:  [  
                    IconButton(
                      onPressed: (){
                           Navigator.pushNamed(context, RouteNames.profile);
                      }, 
                    icon:const Icon(Icons.arrow_back)),
                   const SizedBox(width: 20,),
                   const  Text('All BookMarks',style: TextStyle(fontSize: 25),)
                ],
              ),
            ),
           const SizedBox(height: 200,),
            Container(  
             width: 270,
            alignment: Alignment.center,
              child: Row(  
                children: const [  
                  Icon(Icons.bookmark_add_outlined,color: Colors.grey,),
                  SizedBox(width: 10,),
                  Text('Add your favourite places',style: TextStyle(fontSize: 20,color: Colors.grey),)
                ],
              ),
            ),
           const SizedBox(height: 20,),
            const SizedBox(  
              child:  Text('Add your favourite places',style: TextStyle(fontSize: 20,color: Colors.grey),)
,
            ),
           const SizedBox(height: 30,),
                 Container(  
             width: 270,
            alignment: Alignment.center,
              child: Column(  
                children: const [  
                  Icon(Icons.add_circle,color: Colors.black,),
                  SizedBox(height: 20,),
                  Text('Add Places',style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  Divider( 
                    height: 2,
                
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}