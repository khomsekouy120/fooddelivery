import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_text_field.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class Location extends StatefulWidget{
  const Location({super.key});
  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(30),
                      child: Container(
                        width: 90,
                        height: 90,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(360),
                          image: const DecorationImage(
                            image: AssetImage("assets/images/splash_logo1.png"),
                          )
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.1),
                              offset: Offset(0, 0),
                              blurRadius: 9,
                              spreadRadius: 0,
                            )
                          ]

                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              IconButton(
                                onPressed: (){
                                  Navigator.pop(context);
                                }, 
                                icon: const Icon(Icons.arrow_back_ios, size: 30, color: Colors.black,
                                ),
                              ),
                              TextButton(onPressed: (){}, child: const Text("Deliver to", style: TextStyle(color: Colors.black, fontSize: 22, fontWeight: FontWeight.bold, letterSpacing: 1),))
                            ],
                          ),
                          const SizedBox(height: 20,),
                          const AppTextField(
                                prefixIcon: Icon(Icons.search_rounded),
                                hintText: "Search Location",
                          ),
                          InkWell(
                            onTap: (){Navigator.pushNamed(context, RouteNames.verification_code);},
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: const EdgeInsets.all(10),
                                  height: 50,
                                  width: 50,
                                  decoration: const BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage("assets/images/home.png")
                                      )
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text("Agus sopandi", style: TextStyle(fontSize:18, color: Colors.black, fontWeight: FontWeight.bold, letterSpacing: 1),),
                                    const SizedBox(height: 5,),
                                    SizedBox(width: 280, child: Text("21-42-34, Jelupang, Hyderabad, 500072", style: TextStyle(fontSize: 18, color: Colors.grey[700],),))
                                  ],
                                )
                                // Container(
                                //   width: double.infinity,
                                // )
                              ],
                            ),
                          ),

                          const SizedBox(height: 20,),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: (){Navigator.pushNamed(context, RouteNames.verification_code);},
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.all(10),
                                      height: 50,
                                      width: 50,
                                      decoration: const BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage("assets/images/goal.png")
                                          )
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const Text("Current Location", style: TextStyle(fontSize:18, color: Colors.black, fontWeight: FontWeight.bold, letterSpacing: 1),),
                                        const SizedBox(height: 5,),
                                        SizedBox(width: 280, child: Text("21-42-34, Jelupang, Hyderabad, 500072", style: TextStyle(fontSize: 18, color: Colors.grey[700],),))
                                      ],
                                    )
                                    // Container(
                                    //   width: double.infinity,
                                    // )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 20,),
                          InkWell(
                            onTap: (){Navigator.pushNamed(context, RouteNames.verification_code);},
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: const EdgeInsets.all(10),
                                  height: 50,
                                  width: 50,
                                  decoration: const BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage("assets/images/finding.png")
                                      )
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text("Look up the map", style: TextStyle(fontSize:18, color: Colors.black, fontWeight: FontWeight.bold, letterSpacing: 1),),
                                    // SizedBox(height: 5,),
                                    // Container(width: 280, child: Text("21-42-34, Jelupang, Hyderabad, 500072", style: TextStyle(fontSize: 18, color: Colors.grey[700],),))
                                  ],
                                )
                                // Container(
                                //   width: double.infinity,
                                // )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                            onPressed: (){},
                            child: Text("Save Address", style: TextStyle(color: Colors.grey[800], fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),)
                        ),
                        Row(
                          children: [
                            TextButton(
                                onPressed: (){},
                              style: TextButton.styleFrom(
                                        padding: const EdgeInsets.only(right: 10),
                                        minimumSize: const Size(10, 10),
                                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                        alignment: Alignment.centerLeft),
                                child: Text("Edit", style: TextStyle(color: Colors.grey[800], fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1),)
                            ),
                            Container(
                              width: 40,
                              height: 20,
                              decoration: const BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("assets/images/editeimg.png")
                                )
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap:(){
                            Navigator.pushNamed(context, RouteNames.home);
                          },
                          child: Container(
                            width: 110,
                            height: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  height: 70,
                                  width: 130,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    image: const DecorationImage(
                                      image: AssetImage("assets/images/home.png"),
                                    ),
                                  ),
                                ),
                                const Text("HOME", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
                              ],
                            ),
                          )
                        ),
                        InkWell(
                          onTap:(){
                            Navigator.pushNamed(context, RouteNames.home);
                          },
                          child: Container(
                            width: 110,
                            height: 100,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Column(
                              children: [
                                Container(
                                  height: 70,
                                  width: 130,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: const Icon(Icons.personal_injury_outlined, size: 36,),
                                ),
                                const Text("OFFICE", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
                              ],
                            ),
                          )
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}