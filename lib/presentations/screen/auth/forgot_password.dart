import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/components/app_text_field.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});
  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final formKey = GlobalKey<FormState>();
  final phoneCtrl = TextEditingController();

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container( 
          width: double.infinity,
          height: double.infinity, 
          color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 10),
                width: double.infinity,
                height: 300,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)
                  ),
                  boxShadow: [BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 3),
                    offset: Offset(0, 0),
                    blurRadius: 0,
                    spreadRadius: 0,
                  )]
                ),
                child: Container(
                  width: double.infinity,
                  height: 100,
                  decoration: const BoxDecoration(  
                    image: DecorationImage(  
                      image: AssetImage("assets/images/loginimg.png"),
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(400),
                      bottomRight: Radius.circular(400),
                    )
                  ),
                ),
              ),
              Container(
                padding:const EdgeInsets.all(30),
                child: Column(  
                  children: [
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Forgot", style: TextStyle(color: Colors.purple, fontSize: 30, fontWeight: FontWeight.bold, letterSpacing: 1,),),
                    ),
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text("password?", style: TextStyle(color: Colors.purple, fontSize: 30, fontWeight: FontWeight.bold, letterSpacing: 1,),),
                    ),
                    const SizedBox(height: 30,),
                    Form(
                      key: formKey,
                      child: Column(children: [
                        AppTextField(
                        controller: phoneCtrl,
                        hintText: "Enter your phone number",
                        borderRadius: 5,
                        inputType: TextInputType.number,
                        prefixIcon: const Icon(Icons.phone_android_rounded, color: Colors.grey,),
                        errorText: "Please input this field",
                      ),
                      const SizedBox(height: 10,),
                      const Text("We will send you a message to set or reset your new password", style: TextStyle(color: Colors.grey, fontSize: 14),),
                      const SizedBox(height: 30,),
                      AppButton(
                        text: "Send", 
                        onClick: (){
                          if (formKey.currentState!.validate() &&
                              phoneCtrl.text.isNotEmpty
                          ){
                              return Navigator.pushNamed(context, RouteNames.verification_code);
                          }
                            // }
                        },
                        width: double.infinity, 
                        backgroundColor: Colors.purple,
                      )
                    ],))
                  ],
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}
