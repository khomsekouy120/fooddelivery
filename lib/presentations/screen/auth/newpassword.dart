import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';
import '../../components/app_text_field.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final newPsw = TextEditingController();
    final confirmPwdCtrl = TextEditingController();
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 227, 227, 227),
        body: Padding(
          padding: const EdgeInsets.all(0),
          child: Column(children: [
            Container(
              height: 280,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.white70,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Image.asset(
                            'assets/images/loginimg.png',
                            width: double.infinity,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ]),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  // first tab bar view widget
                  SingleChildScrollView(
                    padding: const EdgeInsets.all(20),
                    child: Expanded(
                      child: Form(
                        key: formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: kDefaultPadding * 2,
                            ),
                            Container(
                              padding: const EdgeInsets.all(0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  Text(
                                    'NEW PASSWORD',
                                    style: TextStyle(
                                        fontSize: 30,
                                        color:
                                            Color.fromARGB(255, 106, 26, 113),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: newPsw,
                              prefixIcon: const Icon(Icons.password),
                              hintText: "********",
                              errorText: "Please enter your new password",
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: confirmPwdCtrl,
                              prefixIcon: const Icon(Icons.password_sharp),
                              hintText: "********",
                              errorText: "Confirm password",
                              isPassword: true,
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            Container(
                              padding: const EdgeInsets.all(0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  Text(
                                    'Your password must be at-least 8 characters long.',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: kDefaultPadding / 1),
                            AppButton(
                                width: double.infinity,
                                text: "Reset password",
                                backgroundColor:
                                    Color.fromARGB(255, 106, 26, 113),
                                onClick: () {
                                  if (formKey.currentState!.validate() &&
                                      newPsw.text.isNotEmpty &&
                                      confirmPwdCtrl.text.isNotEmpty) {}
                                  if (confirmPwdCtrl.text != newPsw.text) {
                                    return ScaffoldMessenger.of(context)
                                        .showSnackBar(
                                      const SnackBar(
                                        content: Text("Password not match"),
                                      ),
                                    );
                                  } else {
                                    Navigator.pushNamed(
                                        context, RouteNames.login);
                                  }
                                }),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
            ),
          ]),
        ));
  }
}
