import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';

import '../../route/route_name.dart';

class VerificationCode extends StatefulWidget{
  const VerificationCode({super.key});
  @override
  State<VerificationCode> createState() => _VerificationCodeState();
}

class _VerificationCodeState extends State<VerificationCode> {
  final formKey = GlobalKey<FormState>();
  final verifyPwdCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey[200],
        child: SingleChildScrollView(
          child: Column(
            children: [
              //Top Bar Image preview
              Container(
                //Container
                padding: const EdgeInsets.only(bottom: 10),
                width: double.infinity,
                height: 300,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)
                    ),
                    boxShadow: [BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 3),
                      offset: Offset(0, 0),
                      blurRadius: 0,
                      spreadRadius: 0,
                    )]
                ),

                //Image
                child: Container(
                  width: double.infinity,
                  height: 100,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/loginimg.png"),
                      ),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(400),
                        bottomRight: Radius.circular(400),
                      )
                  ),
                ),
              ),

              //Body
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(30),
                child: Column(
                  children: [
                    //Title body
                    const SizedBox(height: 20,),
                    const Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Verification Code", style: TextStyle(color: Colors.purple, fontSize: 30, fontWeight: FontWeight.bold, letterSpacing: 1),),
                    ),
                    const SizedBox(height: 20,),
                    //Form user input
                    Container(
                      padding: const EdgeInsets.all(30),
                      width: double.infinity,
                      child: Form(
                        key: formKey,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 50,
                                child: TextFormField(
                                  inputFormatters: [ LengthLimitingTextInputFormatter(1)],
                                  style: const TextStyle(fontSize: 30),
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  //Validation
                                  validator: (value) {
                                    if(value == "" || value == null){
                                      return "";
                                    }
                                    return null;
                                  },
                                ),
                            ),
                            const SizedBox(width: 20,),
                            SizedBox(
                              width: 50,
                              child: TextFormField(
                                inputFormatters: [ LengthLimitingTextInputFormatter(1)],
                                style: const TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                //Validation
                                validator: (value) {
                                  if(value == "" || value == null){
                                    return "";
                                  }
                                  return null;
                                },
                              ),
                            ),
                            const SizedBox(width: 20,),
                            SizedBox(
                              width: 50,
                              child: TextFormField(
                                inputFormatters: [ LengthLimitingTextInputFormatter(1)],
                                style: const TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                //Validation
                                validator: (value) {
                                  if(value == "" || value == null){
                                    return "";
                                  }
                                  return null;
                                },
                              ),
                            ),
                            const SizedBox(width: 20,),
                            SizedBox(
                              width: 50,
                              child: TextFormField(
                                inputFormatters: [ LengthLimitingTextInputFormatter(1)],
                                style: const TextStyle(fontSize: 30),
                                textAlign: TextAlign.center,
                                keyboardType: TextInputType.number,
                                //Validation
                                validator: (value) {
                                  if(value == "" || value == null){
                                    return "";
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ),
                    const Text("Input the code we have been sent to your phone", style: TextStyle(color: Colors.grey, fontSize: 14, letterSpacing: 1),),
                    const SizedBox(height: 50,),
                    AppButton(
                        text: "Verify",
                        width: double.infinity,
                        backgroundColor: Colors.purple,
                        onClick: (){
                          if (formKey.currentState!.validate()) {
                            // if (verifyPwdCtrl.text.isEmpty) {
                              return Navigator.pushNamed(context, RouteNames.newpassword);
                            // }
                          }
                        }
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}