

import 'package:flutter/material.dart';


import 'package:food_deliveries/core/constant.dart';


import 'package:food_deliveries/presentations/components/app_button.dart';


import '../../components/app_text_field.dart';



import '../../route/route_name.dart';

import '../../route/route_name.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final userNameCtrl = TextEditingController();
    final phoneCtrl = TextEditingController();
    final pwdCtrl = TextEditingController();
    final confirmPwdCtrl = TextEditingController();
    return Scaffold(
        backgroundColor: const Color.fromARGB(255, 227, 227, 227),
        body: Padding(
          padding: const EdgeInsets.all(0),
          child: Column(children: [
            Container(
              height: 280,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.white70,
              ),
              child: SingleChildScrollView(
                // padding: const EdgeInsets.all(0),
                child: Column(
                  children: [
                    Column(children: [
                      Padding(
                        padding: const EdgeInsets.all(0),
                        child: Image.asset(
                          'assets/images/loginimg.png',
                          width: double.infinity,
                          height: 230,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ]),
                    SizedBox(
                      // width: double.infinity,
                      width: 300,
                      child: TabBar(
                        controller: _tabController,
                        labelColor: Colors.black,
                        labelStyle: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                        unselectedLabelColor: Colors.black,
                        tabs: const [
                          Tab(
                            text: 'Login',
                          ),

                          Tab(
                            text: 'Logout',
                          ),
                          // ),
                        ],
                      ),
                    ),
                    // tab bar view here
                  ],
                ),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  // first tab bar view widget
                  SingleChildScrollView(
                    padding: const EdgeInsets.all(20),
                    child: Expanded(
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            const SizedBox(
                              height: kDefaultPadding * 2,
                            ),
                            AppTextField(
                              controller: userNameCtrl,
                              prefixIcon: const Icon(Icons.person),
                              hintText: "Email",
                              errorText: "Please enter your email",
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: pwdCtrl,
                              prefixIcon: const Icon(Icons.lock),
                              hintText: "Password",
                              errorText: "Please enter your password",
                              isPassword: true,
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, RouteNames.forgot_password);
                                },
                                child: const Text('Forget Password')),
                            AppButton(
                              width: double.infinity,
                              text: "Login",
                              backgroundColor:
                                  const Color.fromARGB(255, 106, 26, 113),
                              onClick: () {
                                if (formKey.currentState!.validate()) {
                                  if (userNameCtrl.text.isNotEmpty &&
                                    pwdCtrl.text.isNotEmpty
                                    ){
                                      return Navigator.pushNamed(context, RouteNames.welcome_screen);
                                  }
                                }
                              },
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            const SizedBox(
                              child: Text('or'),
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            Container(
                              height: 55,
                              decoration: BoxDecoration(
                                  color: const Color.fromARGB(255, 15, 104, 181),
                                  borderRadius: BorderRadius.circular(25)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.all(2.0),
                                      child: Icon(
                                        Icons.facebook,
                                        color: Colors.white,
                                        size: 40,
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: const Text('Log in with Facebook',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white)),
                                    )
                                  ]),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              padding: const EdgeInsets.only(right: 20),
                              height: 55,
                              decoration: BoxDecoration(
                                  color: Colors.white70,
                                  borderRadius: BorderRadius.circular(25)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(0),
                                      child: Image.asset(
                                        'assets/images/google.png',
                                        width: 40,
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: const Text('Log in with google',
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Color.fromARGB(
                                                  179, 142, 141, 141))),
                                    )
                                  ]),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  // second tab bar view widget
                  SingleChildScrollView(
                    padding: const EdgeInsets.all(20),
                    child: Expanded(
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            const SizedBox(
                              height: kDefaultPadding * 2,
                            ),
                            SizedBox(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text(
                                    'Register',
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color:
                                            Color.fromARGB(255, 132, 22, 152)),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                        width: 60,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: Colors.white70),
                                        child: Image.asset(
                                          'assets/images/google.png',
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      Container(
                                        width: 60,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: Colors.white70),
                                        child: Image.asset(
                                            'assets/images/facebook.png'),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: userNameCtrl,
                              prefixIcon: const Icon(Icons.person),
                              hintText: "Username",
                              errorText: "Please enter your full name",
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: phoneCtrl,
                              prefixIcon: const Icon(Icons.phone_android),
                              hintText: "Phone number",
                              errorText: "Please enter your phone number",
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: pwdCtrl,
                              prefixIcon: const Icon(Icons.lock),
                              hintText: "Password",
                              errorText: "Please enter your password",
                              isPassword: true,
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            AppTextField(
                              controller: confirmPwdCtrl,
                              prefixIcon: const Icon(Icons.confirmation_num),
                              hintText: "Confirm password",
                              errorText: "Please enter your password Again",
                              isPassword: true,
                            ),
                            const SizedBox(height: kDefaultPadding / 2),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                AppButton(
                                  width: 150,
                                  text: "Sign-up",
                                  backgroundColor:
                                      const Color.fromARGB(255, 106, 26, 113),
                                  onClick: () {
                                    if (formKey.currentState!.validate()) {
                                      if (userNameCtrl.text.isNotEmpty &&
                                          pwdCtrl.text.isEmpty) {
                                        return const Text("Logout Success");
                                      }
                                    }
                                  },
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                const Text('Already a Member?'),
                                TextButton(
                                    onPressed: () {},
                                    child: const Text('Login'))
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ));
  }
}
