import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class CustomerFeedback extends StatefulWidget {
  const CustomerFeedback({super.key});

  @override
  State<CustomerFeedback> createState() => _CustomerFeedbackState();
}

class _CustomerFeedbackState extends State<CustomerFeedback> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(  
     body: SingleChildScrollView(  
      child: Column(  
        children: [  
          Container(  
            padding: const EdgeInsets.all(15),
            margin: const EdgeInsets.only(top: 40),
            width: double.infinity,
            child: Column( 
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start, 
              children: [  
                Container(  
                  width: double.infinity,
                  child: Row(  
                    children:  [  
                      IconButton(
                        onPressed: (){ 
                            Navigator.pushNamed(context, RouteNames.profile);
                        }, 
                        icon: const Icon(Icons.arrow_back)),
                    const  SizedBox(width: 15,),
                    const  Text('FostFood Bot', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),)
                    ],
                  ),
                ),
               const Divider(  
                  height: 30,
                  thickness: 2,
                ),
                 Container(  
                      margin: const EdgeInsets.only(left: 90,top: 50),
                    alignment: Alignment.center,
                    width: 200,
                  decoration:  BoxDecoration( 
                  borderRadius: BorderRadius.all(Radius.circular(20)), 
                    border: Border.all(color: Colors.red)
                  ),
                    child: TextButton(
                      onPressed: (){}, 
                      child: const Text("See earlier messages",style: TextStyle(color: Colors.red),)),
                  ),
             const SizedBox(height: 20,),
                Container(  
                  width: double.infinity,
                  child: Row(  
                    mainAxisAlignment:MainAxisAlignment.center,
                    children:const [ 
                      Text('Today at 3:53 Am', style: TextStyle(color: Colors.grey),)
                    ],
                  ),
                ),
              const SizedBox(height: 20,),
              Container( 
                width: double.infinity,
                child: Row( 
                  crossAxisAlignment: CrossAxisAlignment.start, 
                  children: [  
                    Container(  
                      width: 30,
                      height: 30,
                          
                        decoration:const BoxDecoration(  
                          borderRadius:  BorderRadius.all(Radius.circular(25)),
                          image: DecorationImage(
                            image: AssetImage('assets/images/nith.jpg') 
                            ),
                            color: Colors.grey
                        ),
                 
                    ),
                    Container(  
                      margin: const EdgeInsets.only(left: 5),
                      width: 300,
                      padding: const EdgeInsets.all(10),
                      decoration:const BoxDecoration(  
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Color.fromARGB(255, 239, 239, 239)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [  
                          Text.rich( 
                           TextSpan(  
                              text:  'Welcome to ',
                            children: <InlineSpan>[
                          TextSpan(
                            text: ' FastFood Desk ',
                            style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.blue),
                          ),
                         
                        ],
                           )
                          ),
                          Text('Resercation CHat Support. Let us know how we can help you with you booking query, and we\'ll get cracking on it super quick!')
                          
                        ],
                      ),
                    )
                  ],
                ),
              ),
              // now Status
             Container( 
              margin: const EdgeInsets.only(left: 35),
              child: Row(  
                children: const[ 
                  Text('Now',style: TextStyle(color: Colors.grey),)
                ],
              ),
             ),
          // -------------------
             Container( 
                width: double.infinity,
                
                child: Row( 
                  crossAxisAlignment: CrossAxisAlignment.start, 
                  children: [  
                    Container(  
                      width: 30,
                      height: 30,
                        
                        decoration:const BoxDecoration(  
                          borderRadius:  BorderRadius.all(Radius.circular(25)),
                          image: DecorationImage(
                            image: AssetImage('assets/images/nith.jpg') 
                            ),
                            color: Colors.grey
                        ),
                 
                    ),
                    const SizedBox(height: 20,),
                    Container(
                      // padding: const EdgeInsets.all(15),
                         decoration:const BoxDecoration(  
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        color: Color.fromARGB(255, 239, 239, 239)
                      ),
                       width: 300,
                       margin: const EdgeInsets.only(left: 5),
                      child: Container( 
                        child: Column( 
                          children:  [ 
                            Container( 
                              padding: const EdgeInsets.all(10), 
                                child: Column( 
                                  children: const [
                                     Text('Hi Sumanya, please select the order for which you seek support.')
                                  ],
                                ),
                            ),
                           const SizedBox(height: 3,),
                            Container( 
                              padding: const EdgeInsets.only(left: 5,top: 10),
                              width: double.infinity,
                              color: Colors.white,
                              child: Column(  
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [ 
                                         Text.rich( 
                                    TextSpan(  
                                        text:  'Order from ',style: TextStyle(color: Colors.blue,fontSize: 16),
                                      children: <InlineSpan>[
                                    TextSpan(
                                      text: 'Vada Pav Station ',
                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.blue),
                                    ),
                                  
                                  ],
                                    )
                                    ),
                                    Text('Placed on 2nd Nov at 02:16 PM Pav Bhaji with Gravy'),
                                    Text('Your Order was cancelled',style: TextStyle(color: Colors.red),)
                                ],
                              ),
                            ),
                                const SizedBox(height: 3,),
                            Container( 
                              padding: const EdgeInsets.only(left: 5,top: 10),
                              width: double.infinity,
                              color: Colors.white,
                              child: Column(  
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [ 
                                         Text.rich( 
                                    TextSpan(  
                                        text:  'Order from',style: TextStyle(color: Colors.blue,fontSize: 16),
                                      children: <InlineSpan>[
                                    TextSpan(
                                      text: ' The Waffle',
                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.blue),
                                    ),
                                  
                                  ],
                                    )
                                    ),
                                    Text('Placed on 2nd Nov at 02:16 PM Pav Bhaji with Gravy'),
                                    Text('Your Order was cancelled',style: TextStyle(color: Colors.green),)
                                ],
                              ),
                            ),
                                const SizedBox(height: 3,),
                            Container( 
                              padding: const EdgeInsets.only(left: 5,top: 10),
                              width: double.infinity,
                              color: Colors.white,
                              child: Column(  
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: const [ 
                                         Text.rich( 
                                    TextSpan(  
                                        text:  'Order from',style: TextStyle(color: Colors.blue,fontSize: 16),
                                      children: <InlineSpan>[
                                    TextSpan(
                                      text: ' Sandwich Eatery',
                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.blue),
                                    ),
                                  
                                  ],
                                    )
                                    ),
                                    Text('Placed on 2nd Nov at 02:16 PM Pav Bhaji with Gravy'),
                                    Text('Your Order was cancelled',style: TextStyle(color: Colors.green),)
                                ],
                              ),
                            ),
                 

                          ]
                        ), 

                     
                      ),
                      
                    ),
             
                  ],
                ),
              ),

              ],
            ),
          )
        ],
      ),
     ),
    );
  }
}