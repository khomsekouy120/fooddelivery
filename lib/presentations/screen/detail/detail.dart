import 'package:flutter/material.dart';
import 'package:food_deliveries/model/food_menu.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({super.key, required this.id});
  final int id;

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  int amount = 1;
  // int prices = foodmenus[id].price;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          height: 350,
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                  image: AssetImage(
                      "assets/images/home/${foodmenus[widget.id].image}"),
                  fit: BoxFit.cover)),
        ),
        Align(
          alignment: AlignmentDirectional.topCenter,
          child: Container(
            margin: const EdgeInsets.only(top: 30),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios_new,
                      size: 30,
                      color: Colors.white,
                    ))
              ],
            ),
          ),
        ),
        Align(
          alignment: AlignmentDirectional.topCenter,
          child: Container(
            padding: const EdgeInsets.all(30),
            margin: const EdgeInsets.only(top: 320),
            height: double.infinity,
            width: double.infinity,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  foodmenus[widget.id].restaurant,
                  style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.5),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  foodmenus[widget.id].description,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.5),
                ),
                const SizedBox(
                  height: 20,
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    // color: Colors.purple,
                    borderRadius: BorderRadius.circular(30),
                    // border: Border.all(width: 0.5, color: Colors.purple),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "\$${foodmenus[widget.id].price * amount}",
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border:
                                Border.all(width: 0.5, color: Colors.purple)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  if (amount > 0) {
                                    setState(() {
                                      amount--;
                                    });
                                  }
                                },
                                child: const Text(
                                  "-",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 26),
                                )),
                            Text(
                              amount.toString(),
                              style: const TextStyle(
                                  color: Colors.black, fontSize: 16),
                            ),
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    amount++;
                                  });
                                },
                                child: const Text(
                                  "+",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 24),
                                ))
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.purple,
                            borderRadius: BorderRadius.circular(360)),
                        child: const Icon(
                          Icons.local_play_outlined,
                          color: Colors.white,
                          size: 22,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 30,),
                //Botton For Order
                InkWell(
                    onTap: () { },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.circular(15),
                        // border: Border.all(
                        //   width: 1,
                        // )
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text("Order Now", style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),),
                          SizedBox(width: 20,),
                          Icon(Icons.arrow_forward_ios, color: Colors.white, size: 16)
                        ],
                      ),
                    ),
                ),
              ],
            ),
          ),
        ),
        if(foodmenus[widget.id].delivery)
          Align(
            alignment: AlignmentDirectional.topEnd,
            child: Container(
              margin: const EdgeInsets.only(top: 305, right: 30),
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),  
              decoration: BoxDecoration(
                color: Colors.purple,
                borderRadius: BorderRadius.circular(20),

              ),
              child: const Text("FREE DELIVERY", style: TextStyle(color: Colors.white, fontSize: 14),),
            ),
          ),
      ],
    ));
  }
}
