import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
           mainAxisAlignment:MainAxisAlignment.start,
           crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                padding: const EdgeInsets.all(15),
                width: 350,
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 50),
                      child: Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, RouteNames.profile);
                              },
                              icon: const Icon(Icons.arrow_back))
                        ],
                      ),
                    ),
                    
                  const  SizedBox(height: 20,),
                  Container(  
                    width: 350,
                    height: 40,
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [  
                        Text('Settings', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                        SizedBox(height: 5,),
                        Divider( 
                          height: 1,
                        )
                      ],
                    ),
                  ),
                 
                  Container(  
                    width: 350,
                 
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:  [  
                        TextButton(
                          onPressed: (){
                             Navigator.pushNamed(
                                    context, RouteNames.profile);
                          }, 
                          child:const  Text(' Add a Place', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black),),
                          ),
                          const Text('In case we\'re missing something'),
                          const SizedBox(height: 5,),
                        
                       const Divider( 
                          height: 1,
                        )
                     
                      ],
                    ),
                  ),
                               Container(  
                    width: 350,
                 
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:  [  
                        TextButton(
                          onPressed: (){
                             Navigator.pushNamed(
                                    context, RouteNames.profile);
                          }, 
                          child:const  Text(' Places you\'ve added', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black),),
                          ),
                          const Text('See all the places you\'ve added so far'),
                          const SizedBox(height: 5,),
                        
                       const Divider( 
                          height: 1,
                        )
                     
                      ],
                    ),
                  ),
                               Container(  
                    width: 350,
                 
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:  [  
                        TextButton(
                          onPressed: (){
                             Navigator.pushNamed(
                                    context, RouteNames.profile);
                          }, 
                          child:const  Text(' Edit profile', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black),),
                          ),
                          const Text('Change your name, description and profile photo'),
                          const SizedBox(height: 5,),
                        
                       const Divider( 
                          height: 1,
                        )
                     
                      ],
                    ),
                  ),
                               Container(  
                    width: 350,
                 
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:  [  
                        TextButton(
                          onPressed: (){
                             Navigator.pushNamed(
                                    context, RouteNames.profile);
                          }, 
                          child:const  Text(' Notification settings', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black),),
                          ),
                          const Text('Define what alerts and notificatios you want to see'),
                          const SizedBox(height: 5,),
                        
                       const Divider( 
                          height: 1,
                        )
                     
                      ],
                    ),
                  ),
                               Container(  
                    width: 350,
                 
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:  [  
                        TextButton(
                          onPressed: (){
                             Navigator.pushNamed(
                                    context, RouteNames.profile);
                          }, 
                          child:const  Text(' Account settings', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.black),),
                          ),
                          const Text('Change your email or delete your account.'),
                          const SizedBox(height: 5,),
                        
                       const Divider( 
                          height: 1,
                        )
                     
                      ],
                    ),
                  )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
