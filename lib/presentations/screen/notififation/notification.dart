import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class MyNotification extends StatefulWidget {
  const MyNotification({super.key});

  @override
  State<MyNotification> createState() => _MyNotificationState();
}

class _MyNotificationState extends State<MyNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(  
      body: SingleChildScrollView(  
        child:Column(  
          children: [  
            Container( 
              padding: const EdgeInsets.all(15), 
              child:Column(  
                children: [  
                  Container(  
                    margin: const EdgeInsets.only(top: 40),
                    child: Row(  
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [  
                        IconButton(
                          onPressed: (){
                            Navigator.pushNamed(context, RouteNames.profile);
                          },
                           icon:const Icon(Icons.arrow_back)),

                        TextButton(onPressed: (){}, 
                        child: const Text('Mark as Read',style: TextStyle(color: Colors.red),))
                      ],
                    ),
                  ),
                  
                ],
              ),
            ),
             Container(  
              width: double.infinity,
              height: 90,
              color: Color.fromARGB(255, 255, 245, 231),
              child: Row(  
                children: [  
                  Container(  
                    height: 80,
                    width: 110,
                    child: Column(
                      children:[  
                   Image.asset('assets/images/splash_logo1.png')

                      ]  

                    ),
                  ),
                  const SizedBox(width: 15,),

                Container(  
                  width: 250,
                  child: Column(  
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:const [ 
                      SizedBox(  
                        child:  Text('Uh-oh! Your order at Vada Pav Station has been cancelled as requested. we hope to serve you again soon.'),
                      ),
                      SizedBox(height: 5,),
                      SizedBox(  
                        child:   Text('02 Nov',style: TextStyle(color:Colors.grey),),
                      )
                    ],
                  ),
                )
                ],
              ),
             ),
              const SizedBox( height: 15,),
             Container(  
              width: double.infinity,
              height: 90,
              color: Color.fromARGB(255, 254, 254, 254),
              child: Row(  
                children: [  
                  Container(  
                    height: 80,
                    width: 110,
                    child: Column(
                      children:[  
                   Image.asset('assets/images/splash_logo1.png')

                      ]  

                    ),
                  ),
                  const SizedBox(width: 15,),

                Container(  
                  width: 250,
                  child: Column(  
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:const [ 
                      SizedBox(  
                        child:  Text('Tap here to track your order from Vada Pav Station.'),
                      ),
                      SizedBox(height: 5,),
                      SizedBox(  
                        child:   Text('01 Nov',style: TextStyle(color:Colors.grey),),
                      )
                    ],
                  ),
                ),
                
                ],
              ),
             ),
                     const SizedBox( height: 15,),
             Container(  
              width: double.infinity,
              height: 90,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Row(  
                children: [  
                  Container(  
                    height: 80,
                    width: 110,
                    child: Column(
                      children:[  
                   Image.asset('assets/images/splash_logo1.png')

                      ]  

                    ),
                  ),
                  const SizedBox(width: 15,),

                Container(  
                  width: 250,
                  child: Column(  
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:const [ 
                      SizedBox(  
                        child:  Text('Tap here to track your order from Vada Pav Station.'),
                      ),
                      SizedBox(height: 5,),
                      SizedBox(  
                        child:   Text('01 Nov',style: TextStyle(color:Colors.grey),),
                      )
                    ],
                  ),
                ),
                
                ],
              ),
             ),
                     const SizedBox( height: 15,),
             Container(  
              width: double.infinity,
              height: 90,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Row(  
                children: [  
                  Container(  
                    height: 80,
                    width: 110,
                    child: Column(
                      children:[  
                   Image.asset('assets/images/splash_logo1.png')

                      ]  

                    ),
                  ),
                  const SizedBox(width: 15,),

                Container(  
                  width: 250,
                  child: Column(  
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:const [ 
                      SizedBox(  
                        child:  Text('Tap here to track your order from Vada Pav Station.'),
                      ),
                      SizedBox(height: 5,),
                      SizedBox(  
                        child:   Text('01 Nov',style: TextStyle(color:Colors.grey),),
                      )
                    ],
                  ),
                ),
                
                ],
              ),
             ),
             
             
          ],
        ),
      ),
    );
  }
}