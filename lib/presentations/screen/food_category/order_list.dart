import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

import '../../components/app_text_field.dart';

class OrderList extends StatefulWidget {
  const OrderList({super.key});

  @override
  State<OrderList> createState() => _OrderListState();
}

class _OrderListState extends State<OrderList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: 900,
              width: double.infinity,
              // color: Colors.blue,
              child: Column(
                children: [
                  Positioned(
                      child: Container(
                    height: 900,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                      image: AssetImage(
                          'assets/images/fried_rice_more_detail.png'),
                      fit: BoxFit.cover,
                    )),
                    child: Container(
                      margin: const EdgeInsets.only(top: 150),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(20)),
                          color: Colors.white),
                      child: Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text(
                                  'Your Order',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold),
                                ),
                                Container(
                                  child: Row(
                                    children: const [
                                      Icon(Icons.all_inbox_rounded),
                                      Text('2')
                                    ],
                                  ),
                                ),
                                const Icon(Icons.close)
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Container(
                              height: 130,
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                  color: Color.fromARGB(255, 249, 210, 150)),
                              child: Column(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                  'assets/images/home.png'),
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(5),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: const [
                                                    Text(
                                                      'Agus sopandi',
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(
                                                      '21-42-34,jelupang,',
                                                      style: TextStyle(
                                                          color: Colors.grey),
                                                    ),
                                                    Text(
                                                      'Hyderabad, 500072',
                                                      style: TextStyle(
                                                          color: Colors.grey),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        const Spacer(),
                                        Container(
                                            padding: const EdgeInsets.all(5),
                                            child: Row(
                                              children: const [
                                                Text('Edit Address'),
                                              ],
                                            ))
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 35,
                                                width: 35,
                                                decoration: const BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10)),
                                                    color: Colors.orange),
                                                child: const Icon(
                                                    Icons.access_time),
                                              ),
                                              Container(
                                                padding:
                                                    const EdgeInsets.all(5),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: const [
                                                    Text(
                                                      '30 mins',
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        const Spacer(),
                                        Container(
                                            padding: const EdgeInsets.all(5),
                                            child: Row(
                                              children: const [
                                                Text('Schedule time'),
                                              ],
                                            ))
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                              children: [
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 90,
                                              width: 100,
                                              decoration: const BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/fried_rice.png'))),
                                            ),
                                            Container(
                                              // alignment: Alignment.center,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: const [
                                                  Text(
                                                    'Fried Rice',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text('Pista House')
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      Container(
                                        height: 38,
                                        width: 145,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(20)),
                                            border: Border.all(
                                                width: 1,
                                                color: const Color.fromARGB(
                                                    255, 122, 16, 141))),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              TextButton(
                                                  onPressed: () {},
                                                  child: const Text(
                                                    '-',
                                                    style: TextStyle(
                                                        fontSize: 25,
                                                        color: Color.fromARGB(
                                                            255, 122, 16, 141)),
                                                  )),
                                              const Text(
                                                '1',
                                                style: TextStyle(
                                                    fontSize: 25,
                                                    color: Color.fromARGB(
                                                        255, 122, 16, 141)),
                                              ),
                                              TextButton(
                                                  onPressed: () {},
                                                  child: const Text(
                                                    '+',
                                                    style: TextStyle(
                                                        fontSize: 25,
                                                        color: Color.fromARGB(
                                                            255, 122, 16, 141)),
                                                  )),
                                            ]),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(5),
                                        child: const Text('15K'),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 90,
                                              width: 100,
                                              decoration: const BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/baked_jollof.png'))),
                                            ),
                                            Container(
                                              // alignment: Alignment.center,
                                              width: 90,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: const [
                                                  Text(
                                                    'Jollof Rice',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                      'Suhani\'s Stop, Kukatpally')
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      Container(
                                        // alignment: Alignment.center,
                                        height: 38,
                                        width: 145,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(20)),
                                            border: Border.all(
                                                width: 1,
                                                color: const Color.fromARGB(
                                                    255, 122, 16, 141))),

                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            TextButton(
                                                onPressed: () {},
                                                child: const Text(
                                                  '-',
                                                  style: TextStyle(
                                                      fontSize: 25,
                                                      color: Color.fromARGB(
                                                          255, 122, 16, 141)),
                                                )),
                                            const Text(
                                              '1',
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  color: Color.fromARGB(
                                                      255, 122, 16, 141)),
                                            ),
                                            TextButton(
                                                onPressed: () {},
                                                child: const Text(
                                                  '+',
                                                  style: TextStyle(
                                                      fontSize: 25,
                                                      color: Color.fromARGB(
                                                          255, 122, 16, 141)),
                                                )),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(5),
                                        child: const Text('17K'),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 250,
                                        child: const AppTextField(
                                            hintText: 'Enter Promo Code'),
                                      ),
                                      const Spacer(),
                                      AppButton(
                                          text: 'Aplly',
                                          width: 100,
                                          backgroundColor: const Color.fromARGB(
                                              255, 122, 16, 141),
                                          onClick: () {})
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  height: 80,
                                  width: double.infinity,
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      // color: Colors.blue,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color.fromARGB(
                                              153, 234, 234, 234),
                                          offset: Offset(3.0, 3.0),
                                          // blurRadius: 3.0,
                                          spreadRadius: 3.0,
                                        )
                                      ]),
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(
                                          top: 10,
                                          left: 15,
                                          right: 15,
                                        ),
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    child: const Text(
                                                      'Subtotal',
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    child: const Text('35K'),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    child: const Text(
                                                      'Delivery',
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    child: const Text('40K'),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    child: const Text(
                                                      'Total',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    child: const Text(
                                                      '75K',
                                                      style: TextStyle(
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      AppButton(
                                          text: 'Payment',
                                          backgroundColor:
                                              Color.fromARGB(255, 122, 16, 141),
                                          width: 150,
                                          onClick: () {
                                            Navigator.pushNamed(context, RouteNames.payment_method);
                                          }),
                                      // const Spacer(),
                                      AppButton(
                                          text: 'select User',
                                          backgroundColor:
                                              Color.fromARGB(255, 122, 16, 141),
                                          width: 150,
                                          onClick: () {}),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
