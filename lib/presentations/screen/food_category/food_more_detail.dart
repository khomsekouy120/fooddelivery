import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:food_deliveries/presentations/components/app_button.dart';

import '../../route/route_name.dart';

class FoodMoreDetail extends StatefulWidget {
  const FoodMoreDetail({super.key});

  @override
  State<FoodMoreDetail> createState() => _FoodMoreDetailState();
}

class _FoodMoreDetailState extends State<FoodMoreDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Stack(
        children: <Widget>[
          Positioned(
              child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      height: 300,
                      // color: Colors.blue,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/fried_rice_more_detail.png'),
                              fit: BoxFit.cover)),
                      child: Container(
                        padding: const EdgeInsets.only(
                            bottom: 150, left: 14, right: 15),
                        child: Row(
                          children: [
                          IconButton(
                            onPressed: (){
                              Navigator.pushNamed(context, RouteNames.food_detail);
                            },
                             icon: const Icon(Icons.arrow_back_ios_new_outlined,color: Colors.white,)),
                            const Spacer(),
                            Container(
                                height: 30,
                                width: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20)),
                                ),
                                child: Container(
                                    margin:
                                        const EdgeInsets.only(top: 5, left: 10),
                                    child: Row(
                                      children: const [
                                        Icon(
                                          Icons.cyclone_outlined,
                                          color: Colors.white,
                                        ),
                                        Text(
                                          '4.5',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.white,
                                        )
                                      ],
                                    ))),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 500,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                          color: Colors.white60,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                // offset: Offset(5.0, 5.0),
                                // blurRadius: 5.0,
                                spreadRadius: 3.0)
                          ]),
                      child: Container(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Positioned(
                                      child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 15, top: 10),
                                    height: 50,
                                    width: 180,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25)),
                                        // color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.white,
                                              offset: Offset(5.0, 5.0),
                                              // blurRadius: 1.0,
                                              spreadRadius: 2.0)
                                        ]),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: const [
                                        Text(
                                          'Jollof Rice',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20),
                                        ),
                                        Text(
                                          'Suhani\'s Stop, Kukatpally',
                                          style: TextStyle(color: Colors.grey),
                                        )
                                      ],
                                    ),
                                  )),
                                  const Spacer(),
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Colors.red, width: 2),
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(60))),
                                    child: const Icon(
                                      Icons.heart_broken_sharp,
                                      color: Colors.red,
                                      size: 40,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                                child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  child: const Text(
                                    'Description',
                                    style: TextStyle(fontSize: 25),
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  // height: 60,
                                  width: 200,
                                  child: Column(
                                    children: [
                                      const Text('Nutritional Value'),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      const Divider(
                                        height: 2,
                                        color: Colors.black,
                                      ),
                                      Column(
                                        children: [
                                          Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  margin: const EdgeInsets.only(
                                                      top: 15),
                                                  child: Row(
                                                    children: const [
                                                      Text('Protein',
                                                          style: TextStyle(
                                                              fontSize: 12)),
                                                      Spacer(),
                                                      Text('2.5g',
                                                          style: TextStyle(
                                                              fontSize: 12))
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: const [
                                                      Text(
                                                        'Carbohtdrates',
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                      Spacer(),
                                                      Text('14.7g',
                                                          style: TextStyle(
                                                              fontSize: 12))
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: const [
                                                      Text('Soduim',
                                                          style: TextStyle(
                                                              fontSize: 12)),
                                                      Spacer(),
                                                      Text('19%',
                                                          style: TextStyle(
                                                              fontSize: 12))
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: const [
                                                      Text('Potassium',
                                                          style: TextStyle(
                                                              fontSize: 12)),
                                                      Spacer(),
                                                      Text('5%',
                                                          style: TextStyle(
                                                              fontSize: 12))
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: const [
                                                      Text(
                                                          'Rich in Citamina A, C and B3',
                                                          style: TextStyle(
                                                              fontSize: 12)),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      const Divider(
                                        height: 2,
                                        color: Colors.black,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                            Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: const [
                                    Text(
                                      'Rescued',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color.fromARGB(
                                              255, 101, 101, 101)),
                                    ),
                                    Text(
                                      'Vegan',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color.fromARGB(
                                              255, 101, 101, 101)),
                                    ),
                                    Text(
                                      '30 min',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color.fromARGB(
                                              255, 101, 101, 101)),
                                    ),
                                    Text(
                                      '145 cal',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color.fromARGB(
                                              255, 101, 101, 101)),
                                    ),
                                    Text(
                                      'Daliy Value',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color.fromARGB(
                                              255, 101, 101, 101)),
                                    ),
                                  ],
                                )),
                            const SizedBox(
                              height: 15,
                            ),
                            Container(
                                height: 100,
                                padding: const EdgeInsets.all(15),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  color: Colors.white,
                                ),
                                child: ListView(
                                    scrollDirection: Axis.horizontal,
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/carrot.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/olive.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/rice.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/carrot.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/olive.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        height: 90,
                                        width: 90,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100)),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                'assets/images/rice.png',
                                              ),
                                              fit: BoxFit.contain,
                                            )),
                                      ),
                                    ]
                                    // ),
                                    )),
                            const SizedBox(
                              height: 20,
                            ),
                            AppButton(
                                width: double.infinity,
                                text: 'Buy',
                                onClick: () {
                                  Navigator.pushNamed(
                                      context, RouteNames.order_list);
                                })
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ))
        ],
      )),
    );
  }
}
