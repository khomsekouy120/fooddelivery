import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';


class FoodDetail extends StatefulWidget {
  const FoodDetail({super.key});

  @override
  State<FoodDetail> createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  @override
   
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(25),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Fried Rice',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(
                          color: Color.fromARGB(255, 255, 255, 255))),
                  fillColor: Colors.white,
                  filled: true,
                ),

                // ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 15),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Rescued'),
                      onClick: () {},
                      width: 95,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Vegan'),
                      onClick: () {},
                      width: 80,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Delivery'),
                      onClick: () {},
                      width: 95,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('>100'),
                      onClick: () {},
                      width: 75,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 500,
              width: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                color: Colors.white,
              ),
              child: ListView(
               
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        height: 300,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(5.0, 5.0),
                                  blurRadius: 5.0,
                                  spreadRadius: 1.0)
                            ]),
                        child: Column(
                          children: [
                            Container(
                            
                              width: double.infinity,
                              height: 150,
                               child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, RouteNames.food_more_detail);
                                },
                            
 
                              child: Container(
                                  padding: const EdgeInsets.only(top: 20),
                                                               decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20)),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/fried_rice_detail.png'),
                                      fit: BoxFit.cover)),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.only(
                                            top: 15,
                                            left: 5,
                                          ),
                                          width: 80,
                                          height: 50,
                                          decoration: const BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/recued.png'),
                                            ),
                                          ),
                                          child: const Text(
                                            'RESCUED',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.only(
                                            top: 8,
                                            left: 5,
                                          ),
                                          width: 70,
                                          height: 30,
                                          decoration: const BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/discound.png'),
                                            ),
                                          ),
                                          child: const Text(
                                            '50% off',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        const SizedBox(height: 10),
                                        Container(
                                          padding: const EdgeInsets.only(
                                            top: 15,
                                            left: 13,
                                          ),
                                          height: 40,
                                          width: 60,
                                          decoration: const BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/status.png'),
                                            ),
                                          ),
                                          child: const Text(
                                            '30 min',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 10),
                                          ),
                                        ),
                                      ])),
                            ),),
                            Container(
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: const [
                                            Text(
                                              'Suhani Restuarant',
                                              style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.grey,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 7,
                                            ),
                                            Text(
                                              'Chinnese, North Indian',
                                              style: TextStyle(fontSize: 16),
                                            )
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      Container(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 15),
                                        width: 60,
                                        height: 30,
                                        decoration: const BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(20),
                                            ),
                                            color: Colors.green),
                                        child: const Text(
                                          '4.5',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox( height: 10,),
                                  Row(
                                    children: const [
                                          Text('50K', style: TextStyle(fontWeight: FontWeight.bold) ),
                                          Spacer(),
                                          Text('145 cal', style: TextStyle(color:Colors.grey),)
                                    ],
                                  ),
                                  const Divider(   
                                  height: 1,
                                  color: Colors.grey,
                                  ),
                                 const SizedBox( height: 10,),
                                Container( 
                                  width: 400,
                                 child:  const Text(  
                                  ' Left over food and supplies are gathered and sold for 50% off!',
                                  style: TextStyle(color: Colors.grey),
                                 ),
                                )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  
                 
                ],
              ),
            ),
          ],
              
        ),
      ),
    );
  }
}
