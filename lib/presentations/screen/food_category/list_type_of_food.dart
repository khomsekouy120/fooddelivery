import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';

class ListTypeFood extends StatefulWidget {
  const ListTypeFood({super.key});

  @override
  State<ListTypeFood> createState() => _ListTypeFoodState();
}

class _ListTypeFoodState extends State<ListTypeFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(25),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Fried Rice',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(
                          color: Color.fromARGB(255, 255, 255, 255))),
                  fillColor: Colors.white,
                  filled: true,
                ),

                // ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 15),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Rescued'),
                      onClick: () {},
                      width: 95,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Vegan'),
                      onClick: () {},
                      width: 80,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('Delivery'),
                      onClick: () {},
                      width: 95,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AppButton(
                      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
                      textColor: Colors.white,
                      borderColor: Colors.white,
                      text: ('>100'),
                      onClick: () {},
                      width: 75,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 500,
              width: double.infinity,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: Colors.white),
              child: ListView(
       
                children: <Widget>[
                    Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(   
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [   
                        Image.asset('assets/images/breakfirst.png', 
                        width: 70,
                        height: 70,
                        ),
                        Container(  
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(  
                               crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start, 
                            children:const [  
                              Text('Fried Rice - Delivery',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                              Text('Dish')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                    Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(   
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [   
                        Image.asset('assets/images/breakfirst.png', 
                        width: 70,
                        height: 70,
                        ),
                        Container(  
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(  
                               crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start, 
                            children:const [  
                              Text('Fried Rice - Delivery',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                              Text('Dish')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                
                    
                
                    Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(   
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [   
                        Image.asset('assets/images/breakfirst.png', 
                        width: 70,
                        height: 70,
                        ),
                        Container(  
                          width: 300,
                          alignment: Alignment.center,
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(  
                               crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start, 
                            children:const [  
                              Text('Sri Matha Chinese Fast Food Centre',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                              Text('Medcha Road')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                    Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(   
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [   
                        Image.asset('assets/images/breakfirst.png', 
                        width: 70,
                        height: 70,
                        ),
                        Container(  
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(  
                               crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start, 
                            children:const [  
                              Text('Steam fish with lemon',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                              Text('Medchal Road')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                    Container(
                    padding: const EdgeInsets.all(15),
                    child: Row(   
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [   
                        Image.asset('assets/images/breakfirst.png', 
                        width: 70,
                        height: 70,
                        ),
                        Container(  
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(  
                               crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start, 
                            children:const [  
                              Text('Chinese Hub',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                              Text('Kukatpally')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


// positon 