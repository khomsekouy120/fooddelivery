import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';
class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                    padding: const EdgeInsets.only(top: kDefaultPadding * 2, right: 20),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, RouteNames.splash);
                      },
                      child: Text("Skip >>",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(
                                  color:  Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold)),
                )),
              ],
            ),
            const SizedBox(
              height: kDefaultPadding * 2,
            ),
            Container(
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                image: const DecorationImage(
                  image: AssetImage("assets/images/splash_logo1.png"),
                ),
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.circular(100),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(
                      5.0,
                      5.0,
                    ), //Offset
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                  ), //BoxShadow
                  BoxShadow(
                    color: Colors.white,
                    offset: Offset(0.0, 0.0),
                    blurRadius: 0.0,
                    spreadRadius: 0.0,
                  ), //BoxShadow
                ],
              ),
            ),
              const SizedBox(
              height: kDefaultPadding * 2,
            ),
            Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    'Welcome To Food Delivery App',
                    style: TextStyle(
                        fontSize: 45,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: kDefaultPadding * 2,
            ),
            Column( 
              children: [ 
                Container(
                  padding: const EdgeInsets.only(right: 75),
                  child:Row( 
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:const [
                      Text('SELECT LOCATION',
                  style: TextStyle(fontSize: 25,color:Colors.grey),
                  ),
                    ],
                  ) 
                ),
                            const SizedBox(
            height: kDefaultPadding * 2,
            ),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container( 
                    height: 60,
                    decoration: BoxDecoration( 
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                            TextButton.icon(onPressed: (){
                              Navigator.pushNamed(context,RouteNames.delivery_map);
                            }, 
                            icon:const Icon(
                              Icons.search,color:Color.fromARGB(255, 106, 4, 150),size: 35), 
                              label: const Text(
                                'Look in Map',
                                style: TextStyle(
                                  color: Color.fromARGB(255, 106, 4, 150),fontSize: 20),
                             ),
                            )
                    ]),
                  ),
                ),
                   const SizedBox(
              height: kDefaultPadding * 1,
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container( 
                height: 60,
                decoration: BoxDecoration( 
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white 
                ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      TextButton.icon(
                        onPressed: (){
                          Navigator.pushNamed(context, RouteNames.location);
                        }, 
                        icon:const Icon(
                          Icons.location_on,color:Color.fromARGB(255, 106, 4, 150),
                          size: 35,), label: const Text('Your Location',
                          style: TextStyle(color: Color.fromARGB(255, 106, 4, 150),fontSize: 20),
                        )),
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
