import 'package:flutter/material.dart';
import 'package:food_deliveries/core/constant.dart';

import '../../route/route_name.dart';
class SearchFood extends StatefulWidget {
  const SearchFood({super.key});

  @override
  State<SearchFood> createState() => _SearchFoodState();
}

class _SearchFoodState extends State<SearchFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Container(
            // padding: const EdgeInsets.all(40),
            // child: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            // children: [
            //   Container(
            //        child: TextField(
            //       decoration: InputDecoration(
            //           prefixIcon: const Icon(Icons.search),
            //           hintText: 'Book Title',
            //           border: OutlineInputBorder(
            //               borderRadius: BorderRadius.circular(20),
            //               borderSide:
            //                   const BorderSide(color: Colors.grey))),
            //     ),
            //   ),
            // ],
            // ),
            Container(
              padding: const EdgeInsets.all(25),
              child: TextField(
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Search food',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(
                          color: Color.fromARGB(255, 255, 255, 255))),
                  fillColor: Colors.white,
                  filled: true,
                ),

                // ),
              ),
            ),
            Container(
              height: 600,
              width: double.infinity,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: Colors.white),
              child: Column(
                children: [
                  const SizedBox(
                    height: kDefaultPadding * 2,
                  ),
                  Container(
                      padding: const EdgeInsets.only(left: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            'Recent Searches',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  const SizedBox(
                    height: kDefaultPadding / 2,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Row(
                      children: [
                        Row(
                          children: const [Icon(Icons.search), Text('pizza')],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: kDefaultPadding / 2,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Row(
                      children: [
                        Row(
                          children: const [
                            Icon(Icons.search),
                            Text('Frice rice')
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: kDefaultPadding / 2,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 30),
                    child: Row(
                      children: [
                        Row(
                          children: const [
                            Icon(Icons.search),
                            Text('steam fish with lemon')
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: kDefaultPadding * 3,
                  ),
                  Container(
                      padding: const EdgeInsets.only(left: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            'Popular Cuisines',
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  Expanded(
                    child: GridView(
                      padding: const EdgeInsets.all(8),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                      ),
                      children: [
                        Container(
                           alignment: Alignment.center,
                            child: Column(
                          children: [
                            Container(
                               alignment: Alignment.center,
                                child: InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(context,RouteNames.food_type);
                                    },
                                    child: Container(
                                        width: 70,
                                        height: 70,
                                        decoration: const BoxDecoration(
                                            image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/breakfirst.png'),
                                          fit: BoxFit.cover,
                                        ))))),
                            const Text('break First')
                          ],
                        )),
                        Container(
                           alignment: Alignment.center,
                            child: Column(
                          children: [
                            Container(
                               alignment: Alignment.center,
                                child: InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(context,RouteNames.food_type);
                                    },
                                    child: Container(
                                        width: 70,
                                        height: 70,
                                        decoration: const BoxDecoration(
                                            image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/breakfirst.png'),
                                          fit: BoxFit.cover,
                                        ))))),
                            const Text('Pizza')
                          ],
                        )),
                        Container(
                           alignment: Alignment.center,
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                               alignment: Alignment.center,
                                child: InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(context,RouteNames.food_type);
                                    },
                                    child: Container(
                                        width: 70,
                                        height: 70,
                                        decoration: const BoxDecoration(
                                            image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/breakfirst.png'),
                                          fit: BoxFit.cover,
                                        ))))),
                            const Text('eggs ')
                          ],
                        )),
                     Container(
                           alignment: Alignment.center,
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                               alignment: Alignment.center,
                                child: InkWell(
                                    onTap: () {
                                       Navigator.pushNamed(context,RouteNames.food_type);
                                    },
                                    child: Container(
                                        width: 70,
                                        height: 70,
                                        decoration: const BoxDecoration(
                                            image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/breakfirst.png'),
                                          fit: BoxFit.cover,
                                        ))))),
                            const Text('eggs ')
                          ],
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
