import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/components/app_map.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';
import '../../../core/constant.dart';

class DeliveryMap extends StatefulWidget {
  const DeliveryMap({super.key});
  @override
  State<DeliveryMap> createState() => _DeliveryMapState();
}

class _DeliveryMapState extends State<DeliveryMap> {
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 106, 4, 150),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              height: kDefaultPadding * 2,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 70,
                  height: 70,
                  decoration: BoxDecoration(
                      image: const DecorationImage(
                        image: AssetImage('assets/images/splash_logo1.png'),
                      ),
                      border: Border.all(color: Colors.white, width: 2),
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(5.0, 5.0),
                          blurRadius: 5.0,
                          spreadRadius: 1.0,
                        ),
                        BoxShadow(
                            color: Colors.white,
                            offset: Offset((0.0), 0.0),
                            blurRadius: 0.0)
                      ]),
                ),
              ],
            ),
            const SizedBox(
              height: kDefaultPadding * 2,
            ),
            Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 650,
                  decoration:  BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50)),
                      color: const Color.fromARGB(255, 249, 247, 247),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(0, 3), // changes position of shadow
                          ),
                        ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        height: 400,
                        decoration:  BoxDecoration(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(50),
                                topRight: Radius.circular(50)),
                            color: const Color.fromARGB(255, 249, 247, 247),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: const Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Expanded(
                          child: AppMap(
                            onMapMoveCompleted: (addrName) {
                              setState(() {
                              });
                            },
                            onPinLocation: (position) {
                              setState(() {
                              });
                            },
                          ),
                      ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 250,
                        decoration:  BoxDecoration(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(50),
                                topRight: Radius.circular(50)),
                            color: const Color.fromARGB(255, 249, 247, 247),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: const Offset(0, 3), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 25, 25, 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Image.asset('assets/images/edit.png')
                                ],
                              ),
                            ),
                            Row(

                              children: [
                                Container(
                                  padding: const EdgeInsets.all(8),
                                  child:
                                      Image.asset('assets/images/home.png'),
                                ),
                                Expanded(
                                  
                                  child: Column(
                                   
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: const [
                                      
                                      Text('Camsolution technology',style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold),),
                                      Text(
                                          'Camsolution technology, HV4Q+679, Phnom Penh,cambodai col td(street 27,89,1000)',style: TextStyle(color: Colors.grey),)
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                            height: kDefaultPadding / 2,
                          ),
                          AppButton(
                            width: 250,
                            text: "Confirm Location",
                            backgroundColor: const Color.fromARGB(255, 106, 26, 113),
                            onClick: () {
                              Navigator.pushNamed(context, RouteNames.home);
                            },
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
