import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../route/route_name.dart';

class OrderHistory extends StatefulWidget {
  const OrderHistory({super.key});

  @override
  State<OrderHistory> createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      body: SingleChildScrollView(  
        child: Column( 
               mainAxisAlignment:MainAxisAlignment.start,
           crossAxisAlignment: CrossAxisAlignment.start,
      
        
        children: [ 
      
          Container( 
               padding: const EdgeInsets.all(15),
                width: double.infinity, 
            child: Column(  
              children: [  
                 Container(
                      margin: const EdgeInsets.only(top: 40),
                      child: Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, RouteNames.profile);
                              },
                              icon: const Icon(Icons.arrow_back))
                        ],
                      ),
                    ),
                            
                  const  SizedBox(height: 20,),
                  Container(  
                    width: double.infinity,
                   
                  margin: const EdgeInsets.only(left: 10),
                    child: Column(  
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [  
                        Text('Your Orders', style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
                        SizedBox(height: 5,),
                       
                      ],
                    ),
                  ),
                  const  SizedBox(height: 20,),
                  // card 1
                  Container(  
                    height: 280,
                    width: double.infinity,
                    decoration:  BoxDecoration (  
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(width: 1,color: Colors.grey),
                    ),
                    child: Column(  
                      children: [  
                          Container( 
                            padding:const EdgeInsets.only(top: 15) ,

                            child:  Row( 
                              mainAxisAlignment: MainAxisAlignment.spaceAround, 
                              children: [  
                            Container(
                              child: Row(  
                                children: [  
                                   Container( 
                                    height: 60,
                                    width: 80,
                                     decoration:const BoxDecoration(  
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.grey,
                                      image:  DecorationImage(  
                                        image: AssetImage('assets/images/rice.png',
                                      
                                        ),
                                        fit:BoxFit.cover
                                      )
                                     ),
                                   ),

                                   Container( 
                                    margin: const EdgeInsets.only(left: 10),
                                    width: 150,
                                    child: Column( 
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children:const [  
                                        Text('Vada Pav Station',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                                       Text('Kukatpally, Hyderabad',style: TextStyle(color: Colors.grey),),

                                      ],
                                    ) ,
                                   )
                                  
                                ],
                              ),
                            ),
                            Container(
                              child: Text( '\$ 100.00',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                            )
                           
                              ],
                            )
                             
                        
                          ),
                           const Divider(
                                  height: 33,
                                    thickness: 1,

                           ),

                         Container(  
                              margin: const EdgeInsets.only(left: 10),
                          width: double.infinity,
                          child: Column( 
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start, 
                            children:const [  
                              Text('ITEMS',style: TextStyle(fontSize: 16,color: Colors.grey),),
                                 Text('1 x Pav Bhaji with Gravy ',style: TextStyle(fontSize: 16,color: Colors.black),),
                                 SizedBox(height: 10,),
                              Text('ORDERED ON',style: TextStyle(fontSize: 16,color: Colors.grey),),
                             Text('02 Nov 2022 at 2:16 PM',style: TextStyle(fontSize: 16,color: Colors.black),),
                            ],
                          ),
                        ),
                               const Divider(
                                  height: 33,
                                    thickness: 1,

                           ),
                        Container(  
                          height: 35,
                          child: Row(  
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [  
                              TextButton(
                                onPressed:  (){}, 
                                child: const Text('Rejected',style: TextStyle(fontSize: 16,color: Colors.grey),),

                                ),
                             TextButton.icon(
                              onPressed: (){}, 
                              icon:const Icon(Icons.refresh,color: Colors.grey,), 
                              label: const Text('Repeat Order',style: TextStyle(fontSize: 16,color: Colors.grey),))
                          
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  //card 2
                 const  SizedBox(height: 10,),
                  Container(  
                    height: 280,
                    width: double.infinity,
                    decoration:  BoxDecoration (  
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      border: Border.all(width: 1,color: Colors.grey),
                    ),
                    child: Column(  
                      children: [  
                          Container( 
                            padding:const EdgeInsets.only(top: 15) ,

                            child:  Row( 
                              mainAxisAlignment: MainAxisAlignment.spaceAround, 
                              children: [  
                            Container(
                              child: Row(  
                                children: [  
                                   Container( 
                                    height: 60,
                                    width: 80,
                                     decoration:const BoxDecoration(  
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      color: Colors.grey,
                                      image:  DecorationImage(  
                                        image: AssetImage('assets/images/rice.png',
                                      
                                        ),
                                        fit:BoxFit.cover
                                      )
                                     ),
                                   ),

                                   Container( 
                                    margin: const EdgeInsets.only(left: 10),
                                    width: 150,
                                    child: Column( 
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children:const [  
                                        Text('Vada Pav Station',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                                       Text('Kukatpally, Hyderabad',style: TextStyle(color: Colors.grey),),

                                      ],
                                    ) ,
                                   )
                                  
                                ],
                              ),
                            ),
                            Container(
                              child: Text( '\$ 100.00',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                            )
                           
                              ],
                            )
                             
                        
                          ),
                           const Divider(
                                  height: 33,
                                    thickness: 1,

                           ),

                         Container(  
                              margin: const EdgeInsets.only(left: 10),
                          width: double.infinity,
                          child: Column( 
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start, 
                            children:const [  
                              Text('ITEMS',style: TextStyle(fontSize: 16,color: Colors.grey),),
                                 Text('1 x Pav Bhaji with Gravy ',style: TextStyle(fontSize: 16,color: Colors.black),),
                                 SizedBox(height: 10,),
                              Text('ORDERED ON',style: TextStyle(fontSize: 16,color: Colors.grey),),
                             Text('02 Nov 2022 at 2:16 PM',style: TextStyle(fontSize: 16,color: Colors.black),),
                            ],
                          ),
                        ),
                               const Divider(
                                  height: 33,
                                    thickness: 1,

                           ),
                        Container(  
                          height: 35,
                          child: Row(  
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [  
                              TextButton(
                                onPressed:  (){}, 
                                child: const Text('Rejected',style: TextStyle(fontSize: 16,color: Colors.grey),),

                                ),
                             TextButton.icon(
                              onPressed: (){}, 
                              icon:const Icon(Icons.refresh,color: Colors.grey,), 
                              label: const Text('Repeat Order',style: TextStyle(fontSize: 16,color: Colors.grey),))
                          
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

              ],
            ),
          ),
       
          
          
        
        ],
        )
      ),
    );
  }
}