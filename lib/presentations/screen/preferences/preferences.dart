import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class Preferences extends StatefulWidget {
  const Preferences({super.key});

  @override
  State<Preferences> createState() => _PreferencesState();
}

class _PreferencesState extends State<Preferences> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 20),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 70,
                      child: Row(
                        children: [Image.asset('assets/images/home.png')],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          'Home',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '21-42-34, Jelupang,Hyder....',
                          style: TextStyle(color: Colors.grey),
                        )
                      ],
                    )
                  ],
                ),
                Row( 
                  children: [ 
                    IconButton(
                      onPressed: (){},
                       icon: const Icon(Icons.heart_broken_rounded,color: Color.fromARGB(255, 75, 247, 204),size: 40,)
                       )
                  ],
                )
                
              ],
            )),
           //botton search
            Container(  
             
              width: double.infinity,
              height: 40,
              decoration:const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Color.fromARGB(60, 153, 153, 153)
              ),
              child: Row(  
                children: [ 
                  TextButton.icon(
                    
                    onPressed: (){
                      Navigator.pushNamed(context, RouteNames.search_food);
                    }, 
                    icon:const Icon(Icons.search,size: 25,color: Colors.black,),
                     label:const Text('Search food nearby',style: TextStyle(fontSize: 20,color: Colors.grey),))
                ],
              ),
            ),
          
          SingleChildScrollView( 
            child: Column( 
              children: [ 
          const Divider(  
            height: 40,
            thickness: 2,
          ),
          Container(  
            width: double.infinity,
            height: 200,
            decoration:const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Color.fromARGB(57, 0, 122, 78)
              ),
              child: Column( 
                crossAxisAlignment: CrossAxisAlignment.start,
              
                children: [ 
                  Container( 
                    padding: const EdgeInsets.all(10), 
                    child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                
                      children:const [ 
                        Text('Select Prefereces',style: TextStyle(fontSize: 25,color: Colors.white),),
                        Text('for multiple users.',style: TextStyle(fontSize: 16,color: Colors.white),)
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment:CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [ 
                      Image.asset('assets/images/preference.png')
                    ],
                  ),
                 
                ],
              ),
          ),
           const Divider(  
                      height: 30,
                      thickness: 2,
                    ),
         Row(  
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: [ 
             IconButton(
               onPressed: (){
                 Navigator.pushNamed(context, RouteNames.profile);
               },
                icon:const Icon(Icons.arrow_back_ios,color: Color.fromARGB(57, 0, 199, 33),size: 25,)
                ),
           const  Text('Preferensi Anda',style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color:  Color.fromARGB(57, 2, 42, 9)),),
         Image.asset('assets/images/edit.png')
           ],
         ),
        const SizedBox(height: 20,),
         Container(
          height:60 ,
          width: double.infinity,
            decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color.fromARGB(255, 204, 204, 204)
                ),
          child: Row(  
            mainAxisAlignment: MainAxisAlignment.end,
            children: [ 
              Container( 
                  margin: const EdgeInsets.only(right: 10),
                height: 40,
                width: 150,
                decoration: BoxDecoration(
                  borderRadius:const BorderRadius.all(Radius.circular(20)),
                  border: Border.all(width: 1,color:const Color.fromARGB(57, 0, 199, 33))
                ),
                child: Row(
                  children: [ 
                    TextButton(
                      onPressed: (){}, 
                      child:const Text( '-')
                      ),
                    const Text('0'),
                    TextButton(
                      onPressed: (){}, 
                      child:const Text( '+')
                      ),

                  ],
                ),
              )
            ],
          ),
         ),

          const SizedBox(height: 20,),
         const SizedBox( 
            child: Text(' Select active users for current order:',style: TextStyle(fontSize: 20,color: Colors.grey),),
          ),
           const SizedBox(height: 20,),
          // user 1
             Container(
          height:60 ,
          width: double.infinity,
            decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color.fromARGB(255, 204, 204, 204)
                ),
          child: Row(  
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [ 
              const Text( 
                  'User 1', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
              ),
              Container( 
                  margin: const EdgeInsets.only(right: 10),
                height: 40,
                width: 48,
                decoration:const BoxDecoration(
                  borderRadius:BorderRadius.all(Radius.circular(24)),
                  color: Colors.white
                ),
                child: Row(
                  children: [ 
                   IconButton(onPressed: (){

                   }, 
                   icon:const Icon(Icons.add_task_sharp,color: Colors.green,))
                    

                  ],
                ),
              )
            ],
          ),
         ),
         //user2
               const SizedBox(height: 20,),
             Container(
          height:60 ,
          width: double.infinity,
            decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color.fromARGB(255, 204, 204, 204)
                ),
          child: Row(  
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [ 
              const Text( 
                  'User 1', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
              ),
              Container( 
                  margin: const EdgeInsets.only(right: 10),
                height: 40,
                width: 48,
                decoration:const BoxDecoration(
                  borderRadius:BorderRadius.all(Radius.circular(24)),
                  color: Colors.white
                ),
                child: Row(
                  children: [ 
                   IconButton(onPressed: (){}, 
                   icon:const Icon(Icons.add_task_sharp,color: Colors.green,))
                    

                  ],
                ),
              )
            ],
          ),
         ),
         //user3
               const SizedBox(height: 20,),
             Container(
          height:60 ,
          width: double.infinity,
            decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  color: Color.fromARGB(255, 204, 204, 204)
                ),
          child: Row(  
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [ 
              const Text( 
                  'User 1', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
              ),
              Container( 
                  margin: const EdgeInsets.only(right: 10),
                height: 40,
                width: 48,
                decoration:const BoxDecoration(
                  borderRadius:BorderRadius.all(Radius.circular(24)),
                  color: Colors.white
                ),
                child: Row(
                  children: [ 
                   IconButton(onPressed: (){}, 
                   icon:const Icon(Icons.backspace_outlined,color: Colors.red,))
                  ],
                ),
              )
            ],
          ),
         ),
         const SizedBox(height: 20,),
         Column( 
           children: [
             AppButton(text: 'Save Changes', onClick: (){})
           ],
         )
          
             ],
            ),
          )
         
          ],
        ),
      ),
    );
  }
}
