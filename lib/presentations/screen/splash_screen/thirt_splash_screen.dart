import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class ThirtSplashScreen extends StatefulWidget {
  const ThirtSplashScreen({super.key});
  @override
  State<ThirtSplashScreen> createState() => _ThirtSplashScreenState();
}

class _ThirtSplashScreenState extends State<ThirtSplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          color: Colors.purple,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    onPressed: (){
                      Navigator.pushNamed(context, RouteNames.login);
                    }, 
                    child: const Text("Skip  >>", style: TextStyle(color: Colors.white, fontSize: 20, letterSpacing: 1),),
                  ),
                ),
                const SizedBox(height: 40,),
                Container(
                  width: 90,
                  height: 90,
                  decoration: BoxDecoration(  
                    image: const DecorationImage(image: AssetImage("assets/images/splash_logo1.png")),
                    borderRadius: BorderRadius.circular(360),
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                      width: 2
                    )
                  ),
                ),
                const SizedBox(height: 40,),
                const Text("Enjoy The Best Food Experience From Our App", style: TextStyle(color: Colors.white, fontSize: 37), textAlign: TextAlign.center,),
                Container(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: Image.asset("assets/images/3splash.png"),
                ),
                ElevatedButton(
                  onPressed: (){
                    Navigator.pushNamed(context, RouteNames.login);
                  },
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(22)
                      )
                    ),
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    minimumSize: MaterialStateProperty.all(const Size(300, 50))
                  ),
                  child: const Text("GET IN", style: TextStyle(color: Colors.purple, fontSize: 18, letterSpacing: 1),),
                ),
              ],
            ),
          ),
        ),
    );
  }
}
