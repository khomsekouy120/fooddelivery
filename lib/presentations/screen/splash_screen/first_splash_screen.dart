import 'package:flutter/material.dart';
import '../../route/route_name.dart';

class FirstSplashScreen extends StatefulWidget {
  const FirstSplashScreen({super.key});

  @override
  State<FirstSplashScreen> createState() => _FirstSplashScreenState();
}

class _FirstSplashScreenState extends State<FirstSplashScreen> {
  //Time set move to other screen
  @override
  void initState() {
    super.initState();
      Future.delayed(const Duration(seconds: 2), () {
        Navigator.pushNamed(context, RouteNames.second_splash);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20),
        color: Colors.purple,
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                  onPressed: () {
                      Navigator.pushReplacementNamed(context, RouteNames.second_splash);
                  },
                  child: const Text(
                    "Skip  >>",
                    style: TextStyle(
                        color: Colors.white, fontSize: 20, letterSpacing: 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Container(
                width: 90,
                height: 90,
                decoration: BoxDecoration(
                    image: const DecorationImage(
                        image: AssetImage("assets/images/splash_logo1.png")),
                    borderRadius: BorderRadius.circular(360),
                    color: Colors.white,
                    border: Border.all(color: Colors.white, width: 2)),
              ),
              const SizedBox(
                height: 40,
              ),
              const Text(
                "The Best Food Delivery App In Town",
                style: TextStyle(color: Colors.white, fontSize: 37),
                textAlign: TextAlign.center,
              ),
              Image.asset("assets/images/1splash.png")
            ],
          ),
        ),
      ),
    );
  }
}
