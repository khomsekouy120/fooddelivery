import 'package:flutter/material.dart';
import '../../route/route_name.dart';

class SecondSplashScreen extends StatefulWidget {
  const SecondSplashScreen({super.key});
  @override
  State<SecondSplashScreen> createState() => _SecondSplashScreenState();
}

class _SecondSplashScreenState extends State<SecondSplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.pushNamed(context, RouteNames.thirt_splash);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(20),
        color: Colors.purple,
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    onPressed: (){
                      Navigator.pushReplacementNamed(context, RouteNames.thirt_splash);
                    }, 
                    child: const Text("Skip  > >", style: TextStyle(color: Colors.white, fontSize: 20, letterSpacing: 1),),
                  ),
                ),
                const SizedBox(height: 40,),
                Container(
                  width: 90,
                  height: 90,
                  decoration: BoxDecoration(  
                    image: const DecorationImage(image: AssetImage("assets/images/splash_logo1.png")),
                    borderRadius: BorderRadius.circular(360),
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.white,
                      width: 2
                    )
                  ),
                ),
                const SizedBox(height: 40,),
                const Text("Easy To Order Just Click And The Order Will Arrive At Your Location", style: TextStyle(color: Colors.white, fontSize: 34), textAlign: TextAlign.center,),
                Image.asset("assets/images/2splash.png")
              ],
            ),
          ),
        )
      ),
    );
  }
}
