import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class PaymentMethod extends StatefulWidget {
  const PaymentMethod({super.key});

  @override
  State<PaymentMethod> createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 50 ,left: 20),
              width: double.infinity,
             child: Column(  
              crossAxisAlignment:CrossAxisAlignment.start,
              children: [  
                Container(
                     padding: const EdgeInsets.only(right: 15),
                  child:Row(  
                    children:[ 
                      TextButton(
                      onPressed: (){
                        Navigator.pushNamed(context, RouteNames.order_list);
                      },
                      child:const Icon(Icons.arrow_back,color: Colors.black,size: 30,)
                      ),
                      const Spacer(),
                      Container( 
                        padding: const EdgeInsets.only(right: 10),
                        child: Row( 
                          children: [ 
                            TextButton(onPressed: (){ 
                              Navigator.pushNamed(context, RouteNames.profile);
                            },
                             child:const Text( 'Phanith CHHIM'))
                          ],
                        )
                      ),
                      Container( 
                        height: 40, 
                        width: 40,
                     
                        decoration: const BoxDecoration(  
                          color: Colors.grey,
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          image: DecorationImage(  
                            image: AssetImage('assets/images/nith.jpg')
                          )
                        ),
                      ),

                    ],
                  )
                 
                ),
                const Text(
                  'Type Payment',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey),
                )
              ],
             ),
            ),
         
            SizedBox(
              height: 600,
              width: double.infinity,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        height: 60,
                        width: double.infinity,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            color: Color.fromARGB(255, 172, 183, 254)),
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Image.asset('assets/images/cash.png'),
                                const SizedBox(
                                  width: 10,
                                ),
                                const Text(' Cash Payment')
                              ],
                            ),
                            const Spacer(),
                            Row(
                              children: [
                                Checkbox(
                                    value: isChecked,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        isChecked = value!;
                                      });
                                    })
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15,right: 15),
                    child: Container(
                      padding: const EdgeInsets.all(15),
                      height: 60,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Color.fromARGB(255, 172, 183, 254)),
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Image.asset('assets/images/aba.png'),
                              const SizedBox(
                                width: 10,
                              ),
                              const Text('ABA bannk')
                            ],
                          ),
                          const Spacer(),
                          Row(
                            children: [
                              Checkbox(
                                  value: isChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isChecked = value!;
                                    });
                                  })
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15,right: 15, top: 15),
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      height: 60,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          color: Color.fromARGB(255, 172, 183, 254)),
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Image.asset('assets/images/canadia.png'),
                              const SizedBox(
                                width: 10,
                              ),
                              const Text('Canadia Payment')
                            ],
                          ),
                          const Spacer(),
                          Row(
                            children: [
                              Checkbox(
                                  value: isChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isChecked = value!;
                                    });
                                  })
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(  
                    padding: const EdgeInsets.only(left: 15,right: 15,top: 30),
                    child:Row(  
                      children: [
                        AppButton(
                          text: 'Pay Now', 
                          width: 380,
                          backgroundColor:const Color.fromARGB(255, 113, 12, 135),
                        
                        onClick: (){}
                        )
                      ],
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
