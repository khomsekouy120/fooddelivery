
import 'package:flutter/material.dart';
import 'package:food_deliveries/presentations/components/app_button.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
         
          children: [
            Container(
              padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
              child: Row(
                children: [
                  const SizedBox(
                    child: Text(
                      'Personal details',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  const Spacer(),
                  SizedBox(
                    child: TextButton(
                        onPressed: () {},
                        child: const Text(
                          'Edit',
                          style: TextStyle(
                              fontSize: 16,
                              color: Color.fromARGB(255, 121, 0, 154)),
                        )),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.all(15),
              height: 150,
              width: 350,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
              ),
              child: Row(
                children: [
                  Container(
                    height: 90,
                    width: 90,
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 185, 185, 185),
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        image: DecorationImage(
                            image: AssetImage('assets/images/nith.jpg'))),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 5, left: 15),
                    height: 100,
                    width: 200,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: const [
                        Text(
                          'Phanith Chhim',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Text(
                          'phanith1999@gmail.com',
                          style: TextStyle(
                              color: Color.fromARGB(255, 120, 120, 120)),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          '0969432711',
                          style: TextStyle(
                              color: Color.fromARGB(255, 120, 120, 120)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Divider(
                          height: 1,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          '#18-10-2022',
                          style: TextStyle(
                              color: Color.fromARGB(255, 120, 120, 120)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
           const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 60,
                    width: 65,
                  
                    decoration:const BoxDecoration(  
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Colors.white70,
                    ),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: (() {
                                     Navigator.pushNamed(context, RouteNames.bookmaks);
                                }),
                                child: Column(
                                  children: const [
                                    Icon(Icons.bookmark_add_outlined, color: Colors.black,),
                                    Text('Bookmaks',style: TextStyle(fontSize: 10,color: Colors.black),),
                                  ],
                                ))
                          ],
                        )
                      ],
                    ),
                  ),
       
                Container(
                alignment: Alignment.center,
                height: 60,
                width: 80,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: (() {
                                 Navigator.pushNamed(context, RouteNames.notification);
                            }),
                            child: Column(
                              children: const [
                                Icon(Icons.notification_add_outlined, color: Colors.black,),
                                Text('Notifications',style: TextStyle(fontSize: 10,color: Colors.black),),
                              ],
                            ))
                      ],
                    )
                  ],
                ),
              ),
              
                Container(
                alignment: Alignment.center,
                height: 60,
                width: 65,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: (() {
                                   Navigator.pushNamed(context, RouteNames.settings);
                            }),
                            child: Column(
                              children: const [
                                Icon(Icons.settings_applications_outlined, color: Colors.black,),
                                Text('Settings',style: TextStyle(fontSize: 10,color: Colors.black),),
                              ],
                            ))
                      ],
                    )
                  ],
                ),
              ),
         
                Container(
                alignment: Alignment.center,
                height: 60,
                width: 65,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: (() {
                              Navigator.pushNamed(context, RouteNames.payment_method);
                            }),
                            child: Column(
                              children: const [
                                Icon(Icons.payment_outlined, color: Colors.black,),
                                Text('Payments',style: TextStyle(fontSize: 10,color: Colors.black),),
                              ],
                            ))
                      ],
                    )
                  ],
                ),
              )
                ],
              ),
            ),
            //all bottum 1-----------------------------------
            Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                Container(

                alignment: Alignment.center,
                height: 60,
                width: 350,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 300,
                        padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: (() {
                                Navigator.pushNamed(context, RouteNames.order_history);
                              }),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text('Your Order',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                   Spacer(),
                                  Icon(Icons.arrow_forward_ios_outlined, color: Colors.black,),

                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
             
              
          ],
        ),
      ),
      // bottun 2
         Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                Container(

                alignment: Alignment.center,
                height: 60,
                width: 350,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 300,
                        padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: (() {

                                Navigator.pushNamed(context, RouteNames.feedback);
                              }),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text('Feedback & Refunds',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                   Spacer(),
                                  Icon(Icons.arrow_forward_ios_outlined, color: Colors.black,),

                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
              
              
              
          ],
        ),
      ),
      //bottun 3
         Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                Container(

                alignment: Alignment.center,
                height: 60,
                width: 350,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 300,
                        padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: (() {
                                Navigator.pushNamed(context, RouteNames.preferences);
                              }),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text('My Preferences',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                   Spacer(),
                                  Icon(Icons.arrow_forward_ios_outlined, color: Colors.black,),

                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
              
              
          ],
        ),
      ),
      // botton 4
         Container(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                Container(

                alignment: Alignment.center,
                height: 60,
                width: 350,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 300,
                        padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: (() {}),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text('Help',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                   Spacer(),
                                  Icon(Icons.arrow_forward_ios_outlined, color: Colors.black,),

                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
           
              
          ],
        ),
      ),
        Container(  
          width: 350,
          padding: const EdgeInsets.all(15),
          child: Column( 
            crossAxisAlignment: CrossAxisAlignment.start, 
            mainAxisAlignment: MainAxisAlignment.start,
            children: const[  
              Text('Send Feedback',style: TextStyle(fontSize: 20),),
              SizedBox(height: 5,),
              Text('Report an Emergency',style: TextStyle(fontSize: 20),),
               SizedBox(height: 5,),
              Text('Rate us on the Play Store',style: TextStyle(fontSize: 20),),
               SizedBox(height: 5,),
              Text('Log Out',style: TextStyle(fontSize: 20),)
            ],
          )
        ),

          Container(
              padding: const EdgeInsets.all(40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                Container(

                alignment: Alignment.center,
                height: 60,
                width: 150,
              
                decoration:const BoxDecoration(  
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70,
                ),
                child: Row(
                  children: [
                    Container(
                        width: 100,
                        padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: (() {}),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: const [
                                   Icon(Icons.info_outline, color: Colors.black,),
                                Spacer(),
                                  Text('about',style: TextStyle(fontSize: 10,color: Colors.black),),
                                  

                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
           
              
          ],
        ),
      ),
      Container(  
       padding: const EdgeInsets.all(15),
        child: Column(  
          children: [  
            AppButton(text:  'Update',
            width: 350,
            textColor: Colors.white,
            backgroundColor: const Color.fromARGB(255, 108, 2, 127),
             onClick: (){})
          ],
        ),
      ),

 const SizedBox( height: 20,)
          ]
    ),));
      
  }
}
