// import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class OfferedPage extends StatefulWidget{
  const OfferedPage({super.key});

  @override
  State<OfferedPage> createState() => _OfferedPageState();
}

class _OfferedPageState extends State<OfferedPage>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        height: double.infinity,
        width: double.infinity,
        // padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
        child: SizedBox(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Image(image: AssetImage("assets/images/home.png")),
                    const SizedBox(width: 15,),
                    SizedBox(
                      width: 230,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text("Home", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16, letterSpacing: 1),),
                          Text("21-42-34, Jelupang, Hyderabad, 500072 hi ho ho ho", overflow: TextOverflow.ellipsis,)
                        ],
                      ),
                    ),
                    const Spacer(),
                    const Image(image: AssetImage("assets/images/home/Iburger.png"))
                  ],
                ),
              ),
              const SizedBox(height: 20,),
              InkWell(
                onTap: () { },
                child: Container(
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  width: double.infinity,
                  // padding: const EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      Icon(Icons.search, color: Colors.black, size: 22),
                      SizedBox(width: 20,),
                      Text("Search food", style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20,),

              Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                width: double.infinity,
                height: 458,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text("Happy Deals", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),),
                      Text("Relish a big binge with our biggest offers.", style: TextStyle(color: Colors.grey[400], fontSize: 16),),
                      const SizedBox(height: 30,),
                      Container(
                          padding: const EdgeInsets.all(10),
                          width: double.infinity,
                          decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Color.fromARGB(184, 182, 182, 182),
                                offset: Offset(
                                  0.0,
                                  1.0,
                                ),
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                              ),
                            ],
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [Color.fromARGB(255, 180, 57, 30), Color.fromARGB(255, 218, 147, 116)]),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: 155,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text("Rescued Food", style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),),
                                    Text("Saving food and hunger.", style: TextStyle(color: Colors.white, fontSize: 14),),
                                    SizedBox(height: 15,),
                                    Text("Left over food and supplies are gathered from food venders and chefs, and are sold for 50% off!", style: TextStyle(color: Colors.white, fontSize: 14),)
                                  ],
                                )
                              ),
                              SizedBox(
                                width: 195,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 300,
                                      height: 140,
                                      decoration: const BoxDecoration( 
                                        image: DecorationImage(
                                          image: AssetImage("assets/images/home/S2.png"),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),

                      Container(
                        margin: const EdgeInsets.only(top: 30),
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                decoration: BoxDecoration( 
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromARGB(184, 182, 182, 182),
                                      offset: Offset(
                                        0.0,
                                        1.0,
                                      ),
                                      blurRadius: 5.0,
                                      spreadRadius: 1.0,
                                    ),
                                  ],
                                  borderRadius: BorderRadius.circular(20),
                                  gradient: const LinearGradient(  
                                    begin: Alignment.topLeft,
                                    end: Alignment(0.8, 1),
                                    colors: [
                                      Color(0xffca485c),
                                      Color.fromARGB(255, 193, 42, 75),
                                      Color.fromARGB(255, 208, 54, 87),
                                      Color.fromARGB(255, 220, 64, 98),
                                    ]
                                  )
                                ),
                                width: 175,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text("LAAARGE DISCOUNTS", style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),),
                                    const Text("No upper limit!", style: TextStyle(color: Colors.white, fontSize: 14),),
                                    const SizedBox(height: 15,),
                                    Container(
                                      margin: const EdgeInsets.only(left: 60),
                                      width: 100,
                                      height: 80,
                                      decoration: BoxDecoration(  
                                        // color: Color.fromARGB(125, 255, 255, 255),
                                        borderRadius: BorderRadius.circular(1000),
                                        image: const DecorationImage(  
                                          image: AssetImage("assets/images/offers/M1.png"),
                                          fit: BoxFit.cover
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ),
                              SizedBox(
                                width: 175,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                      decoration: BoxDecoration(  
                                        boxShadow: const [
                                          BoxShadow(
                                            color: Color.fromARGB(184, 182, 182, 182),
                                            offset: Offset(
                                              0.0,
                                              1.0,
                                            ),
                                            blurRadius: 5.0,
                                            spreadRadius: 1.0,
                                          ),
                                        ],
                                        borderRadius: BorderRadius.circular(20),
                                        gradient: const LinearGradient(  
                                          begin: Alignment.topLeft,
                                          end: Alignment(0.8, 1),
                                          colors: [
                                            Color.fromARGB(255, 175, 41, 135),
                                            Color.fromARGB(255, 193, 42, 170),
                                            Color.fromARGB(255, 210, 62, 180),
                                            Color.fromARGB(255, 232, 97, 201),
                                          ]
                                        )
                                      ),
                                      width: 175,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          const Text("TRY NEW", style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),),
                                          const Text("Explore unique tastes from new eateries", style: TextStyle(color: Colors.white, fontSize: 14),),
                                          const SizedBox(height: 15,),
                                          Container(
                                            margin: const EdgeInsets.only(left: 60),
                                            width: 95,
                                            height: 90,
                                            decoration: BoxDecoration(  
                                              borderRadius: BorderRadius.circular(360),
                                              // color: Color.fromARGB(101, 255, 255, 255),
                                              image: const DecorationImage(  
                                                image: AssetImage("assets/images/offers/M2.png"),
                                                // fit: BoxFit.fill
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      const SizedBox(height: 20,),
                      Divider(
                        color: Colors.grey[300],
                        height: 20,
                        thickness: 3,
                      ),

                      const SizedBox(height: 30,),
                      const Text("Exclusively on FastFood", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),),
                      Text("Deal-icious offers from top brands!", style: TextStyle(color: Colors.grey[400], fontSize: 16),),


                      SizedBox(
                        width: double.infinity,
                        height: 140,
                        child: ListView(  
                          scrollDirection: Axis.horizontal,
                          children: [
                            Stack(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 60),
                                  width: 170,
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(  
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color.fromARGB(184, 182, 182, 182),
                                        offset: Offset(
                                          0.0,
                                          1.0,
                                        ),
                                        blurRadius: 5.0,
                                        spreadRadius: 1.0,
                                      ),
                                    ],
                                    color: Colors.red[800],
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text("UP TO 33% OFF", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),),
                                      Text("on KFC", style: TextStyle(color: Colors.white, fontSize: 12),)
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: AlignmentDirectional.center,
                                  child: Container(  
                                    margin: const EdgeInsets.fromLTRB(60, 0, 0, 25),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(  
                                      color: Colors.red[300],
                                      borderRadius: BorderRadius.circular(360),
                                      image: const DecorationImage(  
                                        image: AssetImage("assets/images/home/Rkfc.jpeg")
                                      )
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(width: 10,),
                            Stack(
                              children: [
                                Container(
                                  width: 170,
                                  margin: const EdgeInsets.only(top: 60),
                                  padding: const EdgeInsets.all(20),
                                  decoration: BoxDecoration(  
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color.fromARGB(184, 182, 182, 182),
                                        offset: Offset(
                                          0.0,
                                          1.0,
                                        ),
                                        blurRadius: 5.0,
                                        spreadRadius: 1.0,
                                      ),
                                    ],
                                    color: Colors.orange[800],
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text("60% OFF", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),),
                                      Text("on BOWL COMPANY", style: TextStyle(color: Colors.white, fontSize: 12),)
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: AlignmentDirectional.center,
                                  child: Container(  
                                    margin: const EdgeInsets.fromLTRB(60, 0, 0, 25),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(  
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(360),
                                      image: const DecorationImage(  
                                        image: AssetImage("assets/images/home/Rmehfil.jpeg")
                                      )
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(width: 10,),
                            Stack(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 60),
                                  width: 170,
                                  padding: const EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color.fromARGB(184, 182, 182, 182),
                                        offset: Offset(
                                          0.0,
                                          1.0,
                                        ),
                                        blurRadius: 5.0,
                                        spreadRadius: 1.0,
                                      ),
                                    ],  
                                    color: Colors.yellow[800],
                                    borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Text("40% OFF", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),),
                                      Text("on MCDONALD'S", style: TextStyle(color: Colors.white, fontSize: 12),)
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: AlignmentDirectional.center,
                                  child: Container(  
                                    margin: const EdgeInsets.fromLTRB(60, 0, 0, 25),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(  
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(360),
                                      image: const DecorationImage(  
                                        image: AssetImage("assets/images/home/Rburgerking.jpeg")
                                      )
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 30,)                         
                    ]
                  ),
                )
              ),



              Container(
                width: double.infinity,
                height: 66,
                decoration: const BoxDecoration(  
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromARGB(184, 182, 182, 182),
                      offset: Offset(
                        0.0,
                        1.0,
                      ),
                      blurRadius: 5.0,
                      spreadRadius: 1.0,
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    InkWell(
                      onTap: () {
                        
                      },
                      child: Container(  
                        // color: Colors.purple,
                        padding: const EdgeInsets.all(5),
                        width: 40,
                        height: 40,
                        child:  SvgPicture.asset("assets/images/bottom_navbar/Discovery.svg", color: Colors.black,),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        
                      },
                      child: Container(  
                        // color: Colors.purple,
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(  
                          color: Colors.purple,
                          borderRadius: BorderRadius.circular(360)
                        ),
                        width: 40,
                        height: 40,
                        child:  SvgPicture.asset("assets/images/bottom_navbar/Star.svg", color: Colors.white,),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        
                      },
                      child: Container(  
                        padding: const EdgeInsets.all(5),
                        width: 50,
                        height: 50,
                        child: const Icon(Icons.shopping_cart_outlined, size: 30, color: Colors.black87,),
                        // child:  SvgPicture.asset("assets/images/bottom_navbar/Bag.svg", color: Colors.black,),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        
                      },
                      child: Container(  
                        // color: Colors.purple,
                        padding: const EdgeInsets.all(5),
                        width: 40,
                        height: 40,
                        child:  SvgPicture.asset("assets/images/bottom_navbar/User.svg", color: Colors.black,),
                      ),
                    ),
                    
                  ],
                ),
              )
              
            ]
          ),
        ),
      ),
    );
  }
}