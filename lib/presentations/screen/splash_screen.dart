import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:food_deliveries/presentations/route/route_name.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 1), (){
      Navigator.pushNamed(context, RouteNames.first_splash);
    });
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Container(
            width:  double.infinity,
            height: double.infinity,
            decoration:const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/splash.png"),
              ),
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 200,
                    height: 200,
                    decoration: BoxDecoration(
                      image:const DecorationImage(
                        image: AssetImage("assets/images/splash_logo1.png"),
                      ),
                      border: Border.all(
                        color: Colors.white,
                        width: 2
                      ),
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset:  Offset(
                            5.0,
                            5.0,
                          ), //Offset
                          blurRadius: 10.0,
                          spreadRadius: 2.0,
                        ), //BoxShadow
                        BoxShadow(
                          color: Colors.white,
                          offset:  Offset(0.0, 0.0),
                          blurRadius: 0.0,
                          spreadRadius: 0.0,
                        ), //BoxShadow
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 100),
                    child: Container(
                      width: 70,
                      height: 70,
                      decoration:const BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/ellipse.png"),
                        ),
                      ),
                      child: Center(
                        child: IconButton(
                          icon: const Icon(Icons.arrow_forward_ios, color: Colors.white,),
                          onPressed: (){},
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Skip", style: Theme.of(context).textTheme.bodyText2!.copyWith(color: const Color.fromARGB(255, 68, 22, 147), fontSize: 16, fontWeight: FontWeight.bold),),
                  ),
                  const Padding(
                    padding:  EdgeInsets.all(20),
                    child: SpinKitDualRing(
                      color: Colors.purple,
                      size: 30,
                    ),
                  ),
                ],
              ),
            ),
          ),
    );
  }
}